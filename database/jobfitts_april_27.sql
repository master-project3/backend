-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:8111
-- Generation Time: Apr 27, 2021 at 07:32 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.3.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jobfitts`
--

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abn` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `name`, `abn`, `address`, `url`, `contact_no`, `status`, `created_at`, `updated_at`, `email`) VALUES
(3, 'ABC company', '32834437593', 'George Street', 'https://jobfitts.com.au', '0451249110', 1, '2021-04-19 20:19:41', '2021-04-19 20:19:41', ''),
(4, 'ABC company', '90103683776', 'George Street', 'https://jobfitts.com.au', '0451249110', 1, '2021-04-26 13:53:13', '2021-04-26 13:53:13', 'test@test.com');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime DEFAULT NULL,
  `is_full_day_event` tinyint(4) NOT NULL DEFAULT 0,
  `recurring_end_date` datetime DEFAULT NULL,
  `is_recurring` tinyint(4) NOT NULL,
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `duration` int(11) DEFAULT NULL,
  `rrule` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `no_of_employee` int(11) DEFAULT NULL,
  `paid_frequency` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'weekly',
  `base_rate` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_accepted` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `title`, `description`, `start_date`, `end_date`, `is_full_day_event`, `recurring_end_date`, `is_recurring`, `created_by`, `location`, `note`, `status`, `duration`, `rrule`, `client_id`, `no_of_employee`, `paid_frequency`, `base_rate`, `created_at`, `updated_at`, `is_accepted`) VALUES
(1, 'ABC jobs', 'description', '2021-04-26 10:04:00', '2022-04-26 10:04:00', 0, NULL, 1, 'admin@admin.com', '', 'yesy', 1, NULL, 'FREQ=MONTHLY;INTERVAL=2;BYSETPOS=-1;BYDAY=-1MO;UNTIL=20210430T000000', 3, 2, 'weekly', '10000', '2021-04-26 19:26:24', '2021-04-26 19:26:24', 0),
(2, 'ABC jobs', 'description', '2021-04-26 10:04:00', NULL, 0, NULL, 1, 'admin@admin.com', '', 'yesy', 1, NULL, 'FREQ=MONTHLY;INTERVAL=2;BYSETPOS=-1;BYDAY=-1MO;UNTIL=20210430T000000', 3, 2, 'weekly', '10000', '2021-04-26 19:31:21', '2021-04-26 19:31:21', 0);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(2, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(3, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(4, '2016_06_01_000004_create_oauth_clients_table', 1),
(5, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(6, '2021_04_07_051730_create_users_table', 1),
(7, '2021_04_07_051731_create_roles_table', 1),
(8, '2021_04_07_051732_create_role_user_table', 1),
(9, '2021_04_07_051735_create_password_resets_table', 1),
(10, '2021_04_07_051736_create_failed_jobs_table', 1),
(11, '2021_04_20_034800_create_clients_table', 2),
(12, '2021_04_20_043835_alter_clients_table_change_abn_type', 3),
(13, '2021_04_26_004651_create_jobs_table', 4),
(15, '2021_04_26_233824_alter_table_clients_add_email', 5),
(16, '2021_04_27_052454_alter_table_jobs_add_is_accepted', 6);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('0fa5e8c5b6821a832d287f8fe4c3184c874f73a4662ceed09483ddb9dc4f6f016938344adda3042c', 1, 2, NULL, '[]', 0, '2021-04-18 16:17:12', '2021-04-18 16:17:12', '2022-04-19 02:17:12'),
('16f681119f15f75aa19101bbc489b7c3322c8e77bec646de4465f9970498a5326e66bd51978bb988', 1, 2, NULL, '[]', 0, '2021-04-18 16:00:56', '2021-04-18 16:00:56', '2022-04-19 02:00:56'),
('1a9ba8f519c48bb15828c553094b8fc6e8b98c7b6f7324c3b4e3a0182145282c2ee929ecd1670117', 1, 2, NULL, '[]', 0, '2021-04-18 15:57:26', '2021-04-18 15:57:26', '2022-04-19 01:57:26'),
('223539de5dae958c3912bf2138edfc6b275b391554cdd6226baffa8d349aadd4897defef473bc437', 1, 2, NULL, '[]', 0, '2021-04-18 15:56:13', '2021-04-18 15:56:13', '2022-04-19 01:56:13'),
('26b98c575e0d1e19329e8485d03fbdb3be3d7e3e6c74c96a63ca7beca65df5cc2d7f0bd269ff0c8f', 1, 2, NULL, '[]', 0, '2021-04-18 16:19:19', '2021-04-18 16:19:19', '2022-04-19 02:19:19'),
('27351c3fb37a19c1574c9c5dc08b794a1a83e0ac60617f00b1d3d712cc2af45ad2a680d48ff05ecc', 1, 2, NULL, '[]', 0, '2021-04-18 15:49:02', '2021-04-18 15:49:02', '2022-04-19 01:49:02'),
('28f0173123cf4727db96ab681500596a9c6d006a316c2ba55996f30d6623a3589368c8dddb3356b5', 1, 2, NULL, '[]', 0, '2021-04-18 16:22:40', '2021-04-18 16:22:40', '2022-04-19 02:22:40'),
('2dcfe767c5e7697aa1c9d803befd329f7af72c33271fb9b7d34737f1671f63169cb2ba75681cb654', 1, 2, NULL, '[]', 0, '2021-04-19 19:50:02', '2021-04-19 19:50:02', '2022-04-20 05:50:02'),
('2dfd79c08f3edac68f1158291d3a4f54c7b2c0783ccd4cc7a94bd6f02dc8b5274c6b63c5ea90e947', 1, 2, NULL, '[]', 0, '2021-04-18 15:55:23', '2021-04-18 15:55:23', '2022-04-19 01:55:23'),
('343ba4b9543fb733cd7864e7b6623a56970e60e6af9f2e9e8461d56e53576d13c9cd37587a03d203', 1, 2, NULL, '[]', 0, '2021-04-18 15:50:17', '2021-04-18 15:50:17', '2022-04-19 01:50:17'),
('37592d98c809a1735f90ae998c18db2bf9e38efa224a56639b19fe5ddc5c92c9019dbfdb8097d18b', 1, 2, NULL, '[]', 0, '2021-04-18 16:17:51', '2021-04-18 16:17:51', '2022-04-19 02:17:51'),
('38064bd8a191ccae9b20822c3bd80f3782e6c3bfb1463a0365f23586e45b275fb1772f2b3f9283f9', 1, 2, NULL, '[]', 0, '2021-04-18 16:15:42', '2021-04-18 16:15:42', '2022-04-19 02:15:42'),
('3ed3839d20eb89f970c3d811a67786e9f065388a59e6fce7ff27d18b3e03b5b5f8424d592286256c', 1, 2, NULL, '[]', 0, '2021-04-18 15:48:14', '2021-04-18 15:48:14', '2022-04-19 01:48:14'),
('475367e7f14166a0c74c171fafefe236c488e058a7235d3808980b5773b2e9e28cb690478d28244f', 1, 2, NULL, '[]', 0, '2021-04-18 15:51:11', '2021-04-18 15:51:11', '2022-04-19 01:51:11'),
('53f0832f2d3b18a95fa741b83e96ef113d5686124b0a4c01c0488fef60b966ec04c87cf59eb9b399', 1, 2, NULL, '[]', 0, '2021-04-18 16:17:27', '2021-04-18 16:17:27', '2022-04-19 02:17:27'),
('54e0e7c42b789f4a08c87ab8573fb9275092999d762d98a2426a48a65bf0d063c92b4167bc37001a', 1, 2, NULL, '[]', 0, '2021-04-18 16:16:20', '2021-04-18 16:16:20', '2022-04-19 02:16:20'),
('5555fed37e07118e204b31eeea29b4b440fc15de1f30cb92bb00f750af214f96d807e8ea2ca54ae6', 1, 2, NULL, '[]', 0, '2021-04-18 16:18:11', '2021-04-18 16:18:11', '2022-04-19 02:18:11'),
('58b8a8b462e1bc3f46fbdd42b132e61217f4d49a1c329d94a0c7de19017bdbbbae7d140531c370d0', 1, 2, NULL, '[]', 0, '2021-04-18 15:57:15', '2021-04-18 15:57:15', '2022-04-19 01:57:15'),
('58c730ec046a671786203a96102948bcf2df53fd88c4098ba5c39dd9985ac06d2b11e4e088dcd3db', 1, 2, NULL, '[]', 0, '2021-04-26 02:01:18', '2021-04-26 02:01:18', '2022-04-26 12:01:18'),
('64bd97c57652190ec4603c56f1023f6c11278ae4a111e8a2b5ddd8893583d226b60c77784c6b4077', 2, 2, NULL, '[]', 0, '2021-04-19 19:28:44', '2021-04-19 19:28:44', '2022-04-20 05:28:44'),
('669ba7fcf5e9d9e9214e05b6f02e5b114c433a95d6d08986fb6123532887d3e9080503b807c67893', 1, 2, NULL, '[]', 0, '2021-04-18 16:13:30', '2021-04-18 16:13:30', '2022-04-19 02:13:30'),
('6a7ed9f26759bfe06ae24084a15f2893d77026f2cd6f61b2c2a207f8d77e449ccc654dcedf5ea4b5', 1, 2, NULL, '[]', 0, '2021-04-18 15:50:03', '2021-04-18 15:50:03', '2022-04-19 01:50:03'),
('6c9428a359645b855e17e4e87f7f14b6837ae18cee40aacbd37123262ae907be829558a473b2df1c', 2, 2, NULL, '[]', 0, '2021-04-19 14:34:30', '2021-04-19 14:34:30', '2022-04-20 00:34:30'),
('73d04f7bddf1a8a2ce1be7a402f3e86b84a7219af5f9131d84345707f10b2a6e6378729f3ff7b04a', 2, 2, NULL, '[]', 0, '2021-04-18 16:32:27', '2021-04-18 16:32:27', '2022-04-19 02:32:27'),
('7729f561fd3eb595c33fcf67b437723c22649eb1ec08be393f4f802112ac463ef9434ff2cac0b450', 1, 2, NULL, '[]', 0, '2021-04-18 16:14:30', '2021-04-18 16:14:30', '2022-04-19 02:14:30'),
('7a9b70310331074be4bd07c53bc9865cde2fff258ce2570b785c9e1259665d8033b03508b2355f08', 1, 2, NULL, '[]', 0, '2021-04-18 16:15:00', '2021-04-18 16:15:00', '2022-04-19 02:15:00'),
('7cbcfd1913a85db78462fa76fee99d476d0474cb89f996434598fa621d61ceb133cce3a761b44170', 1, 2, NULL, '[]', 0, '2021-04-18 16:02:58', '2021-04-18 16:02:58', '2022-04-19 02:02:58'),
('84ef85755ce65759d001a1feff869d4eaa713e2ab8f04abd29751f36023d137c7b87ec7dde1840d3', 1, 2, NULL, '[]', 0, '2021-04-18 15:54:27', '2021-04-18 15:54:27', '2022-04-19 01:54:27'),
('8599ce819a5e66d58e4b2a760770507c501e7567931015fab90a7400531d02cf59b35262a6222272', 2, 2, NULL, '[]', 0, '2021-04-18 16:28:52', '2021-04-18 16:28:52', '2022-04-19 02:28:52'),
('8fb794a34d4171574d9c6b1fbae8eade694d4ef892d2d9704d11d30447ff848a144acbbd879fe91b', 1, 2, NULL, '[]', 0, '2021-04-18 15:49:36', '2021-04-18 15:49:36', '2022-04-19 01:49:36'),
('91862c2522b429336320ebbc05b29459d10417782f62bb9dab5d9c7b088bd3680cfe8ea6fd290310', 1, 2, NULL, '[]', 0, '2021-04-18 16:16:30', '2021-04-18 16:16:30', '2022-04-19 02:16:30'),
('97dfc0c36bb7a8e3525b06cc245885051064962e9dd7aa12649311559af95aba78861fbd036a83f5', 1, 2, NULL, '[]', 0, '2021-04-18 15:50:54', '2021-04-18 15:50:54', '2022-04-19 01:50:54'),
('9e71051a8f24dcf587b924b94e431f642288132d053881701d53055718b7a0044a3647d967548120', 1, 2, NULL, '[]', 0, '2021-04-22 22:11:11', '2021-04-22 22:11:11', '2022-04-23 08:11:11'),
('9fff008d3ef077427b1e2602e2604176c5e3ae5b2ae5ccecf4623ed82e9f65399a5d237980d1d9fa', 1, 2, NULL, '[]', 0, '2021-04-18 15:57:37', '2021-04-18 15:57:37', '2022-04-19 01:57:37'),
('a0558e1d91c375464e9ec183ca714ca4527357a78f34b58f353bac4eeabc5dda1d93be908134c530', 1, 2, NULL, '[]', 0, '2021-04-18 15:55:51', '2021-04-18 15:55:51', '2022-04-19 01:55:51'),
('a098f4ed28d59741c32dcf47eb5774a8168d935e051ac303a18d2543bbea97046f684f5361417afe', 1, 2, NULL, '[]', 0, '2021-04-18 15:54:03', '2021-04-18 15:54:03', '2022-04-19 01:54:03'),
('a1e0bb2c8c4043d54bff95c3d98321dc888de6717e5b0f8c595aa68fe622dc19d91187357c0bd285', 1, 2, NULL, '[]', 0, '2021-04-18 16:15:33', '2021-04-18 16:15:33', '2022-04-19 02:15:33'),
('a32ebbaaaef6e7b6fb3e865e16a7867b42fafb536f352af4aec0c428d75f381252e11d5021ecdb73', 1, 2, NULL, '[]', 0, '2021-04-18 16:16:04', '2021-04-18 16:16:04', '2022-04-19 02:16:04'),
('b617e5ff7f70c6a3a73a9e9fb7d3afe4a579e2ee9e29a50b93355921de6a7c24c41ac7d7c5e348bc', 1, 2, NULL, '[]', 0, '2021-04-18 16:04:53', '2021-04-18 16:04:53', '2022-04-19 02:04:53'),
('baf9fdfabc7e262d862a81c0048320d847b01b379fee9c7d80dcab8813c0f410b7460dae3fcbc171', 1, 2, NULL, '[]', 0, '2021-04-18 15:59:00', '2021-04-18 15:59:00', '2022-04-19 01:59:00'),
('bd755e15f1ec93b79e561d171955dd867fab5c612f2c868fef433c8276329ec2118e79105a9d6c7e', 1, 2, NULL, '[]', 0, '2021-04-18 15:56:05', '2021-04-18 15:56:05', '2022-04-19 01:56:05'),
('bf7b031f210e2b143df615b42aecb5d9a32944a464b6d32ada2342fc1cea265bd36cb1531b9c3e0a', 1, 2, NULL, '[]', 0, '2021-04-18 15:54:50', '2021-04-18 15:54:50', '2022-04-19 01:54:50'),
('c7834835665a63f4fef4deb060acc711c37d4e5506ab627f37e9f322449ee75f2dc07b3c8e5137ac', 2, 2, NULL, '[]', 0, '2021-04-19 14:50:42', '2021-04-19 14:50:42', '2022-04-20 00:50:42'),
('c8d5328f740f72ccdd57f011654ad83c4f911d16546780f0e644a7ed9afaef808085808698f0b34a', 1, 2, NULL, '[]', 0, '2021-04-18 16:00:45', '2021-04-18 16:00:45', '2022-04-19 02:00:45'),
('cc5ccb0c99f83ab373e5e0460dd80fe580d7415e222c2db348df7be6d144200fd53c03a0e1d72348', 1, 2, NULL, '[]', 0, '2021-04-18 16:06:10', '2021-04-18 16:06:10', '2022-04-19 02:06:10'),
('ce52795cbe991bb654e19985e7fc2a04b38c35920e9ea261a04212c981b229c2689a34fcd1993fa5', 1, 2, NULL, '[]', 0, '2021-04-18 15:59:17', '2021-04-18 15:59:17', '2022-04-19 01:59:17'),
('cf56928dacb8424093bfe53457a8b6b1542259408470bb67a1cdf2ef1a76ad9a8b0baa613e8913cf', 1, 2, NULL, '[]', 0, '2021-04-18 15:56:55', '2021-04-18 15:56:55', '2022-04-19 01:56:55'),
('da2c64f5403fd569fe898c8733e38883c504b37cfbcf010b74bb748964cb6f8cfd6e330b8ad78d42', 1, 2, NULL, '[]', 0, '2021-04-18 16:21:16', '2021-04-18 16:21:16', '2022-04-19 02:21:16'),
('df9bcf1bb3928967282ee889c9c9744ffb9f476af46f9010622217f16696791f398962bd47ce16cd', 1, 2, NULL, '[]', 0, '2021-04-18 15:52:38', '2021-04-18 15:52:38', '2022-04-19 01:52:38'),
('e01cc84e3e43acd2157ac3d6254baa8c8ef96b073a417599e01a0b2272f42f11ffd53efa027dca27', 1, 2, NULL, '[]', 0, '2021-04-18 16:12:59', '2021-04-18 16:12:59', '2022-04-19 02:12:59'),
('e5d6cdea4ee293ddcf77b72b5477e609900fe5f548d23f061d304b3961b10a5879e348135595beaa', 1, 2, NULL, '[]', 0, '2021-04-18 15:58:32', '2021-04-18 15:58:32', '2022-04-19 01:58:32'),
('e8d5344da7a2598c662412a41055b9cdf0e340184e6476aedc53ba35b641a25a22739c3259c314f7', 1, 2, NULL, '[]', 0, '2021-04-18 16:15:15', '2021-04-18 16:15:15', '2022-04-19 02:15:15'),
('eb1448a3a4928637ade8409c61cdda04192143cbf728a821e1cc706f94ba8de5a334820ee3b98f38', 1, 2, NULL, '[]', 0, '2021-04-18 16:01:06', '2021-04-18 16:01:06', '2022-04-19 02:01:06'),
('ed801ba930b148065b1fe58720b241ed97e3a93fa9f03f83b27d9a44677975f8b3571338e706a808', 1, 2, NULL, '[]', 0, '2021-04-18 15:48:34', '2021-04-18 15:48:34', '2022-04-19 01:48:34'),
('f32150baaffd21a0215710ed0f61a53eabf1f01aac7c594979efb5a6a1028f86b2f635250870efff', 1, 2, NULL, '[]', 0, '2021-04-18 16:18:27', '2021-04-18 16:18:27', '2022-04-19 02:18:27'),
('f4d241a1da48b2c7b6f495f96304ca14bd19839519b7548c20492b90e1aaf4cf5d64f3ea0265d66f', 2, 2, NULL, '[]', 0, '2021-04-19 19:11:40', '2021-04-19 19:11:40', '2022-04-20 05:11:40');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', '566zpx8oo5AySTFfAXA0615MVQsJMbxgIkIOaabu', NULL, 'http://localhost', 1, 0, 0, '2021-04-18 15:07:09', '2021-04-18 15:07:09'),
(2, NULL, 'Laravel Password Grant Client', 'EIGi6hHtQXk1vTqpcM8PZdlfO9pjwWSKRVlAMkLU', 'users', 'http://localhost', 0, 1, 0, '2021-04-18 15:07:09', '2021-04-18 15:07:09');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2021-04-18 15:07:09', '2021-04-18 15:07:09');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_refresh_tokens`
--

INSERT INTO `oauth_refresh_tokens` (`id`, `access_token_id`, `revoked`, `expires_at`) VALUES
('04a1ba20a8820e695443ae6317ab49147230ccd19ed3df882727c5fe147dd82c127d1207eb1ea546', 'a32ebbaaaef6e7b6fb3e865e16a7867b42fafb536f352af4aec0c428d75f381252e11d5021ecdb73', 0, '2022-04-19 02:16:04'),
('1413ad31ceb975d42d018d018a813d5f7b1df5540edddf0e935c4e3ef2066a7896ffae9712666e0a', '2dfd79c08f3edac68f1158291d3a4f54c7b2c0783ccd4cc7a94bd6f02dc8b5274c6b63c5ea90e947', 0, '2022-04-19 01:55:23'),
('157d976a56c43721d2a4382f7a66b6a0bb4b59ee5fe17112c1838096553594e061062497d1dca15d', '54e0e7c42b789f4a08c87ab8573fb9275092999d762d98a2426a48a65bf0d063c92b4167bc37001a', 0, '2022-04-19 02:16:20'),
('1d416b1caf7a5317b14af30ee9f846626c38397c84c272dc4727642326820ce65e7b62bb7c54f456', 'bf7b031f210e2b143df615b42aecb5d9a32944a464b6d32ada2342fc1cea265bd36cb1531b9c3e0a', 0, '2022-04-19 01:54:50'),
('1e98f2b4caaefe5454d8cd58dd387122da6491eef7830a06af6d62a4610d7e0ab4b95ef2c0918941', '3ed3839d20eb89f970c3d811a67786e9f065388a59e6fce7ff27d18b3e03b5b5f8424d592286256c', 0, '2022-04-19 01:48:14'),
('2229d140a686577227a2bb1608991418b39abad9652cde27268ddea59c804937c68614f23c019d0b', '223539de5dae958c3912bf2138edfc6b275b391554cdd6226baffa8d349aadd4897defef473bc437', 0, '2022-04-19 01:56:13'),
('276aba54b0525f48d3b2e83bd506dd374e35ca031f303b0aa6816342492fd5218ab7d1f3bf978608', 'da2c64f5403fd569fe898c8733e38883c504b37cfbcf010b74bb748964cb6f8cfd6e330b8ad78d42', 0, '2022-04-19 02:21:16'),
('3566be0bf8e425f053526c917c5c1ff5fd1cbd58065974fd2da81f11c661ac037eec9ed63b1d3d0b', '73d04f7bddf1a8a2ce1be7a402f3e86b84a7219af5f9131d84345707f10b2a6e6378729f3ff7b04a', 0, '2022-04-19 02:32:27'),
('38187eb75f396ac374762fd32f3f24e32c51306dc3c23080d164f7f5f3200109df4cc0e8b39d4d48', '2dcfe767c5e7697aa1c9d803befd329f7af72c33271fb9b7d34737f1671f63169cb2ba75681cb654', 0, '2022-04-20 05:50:02'),
('3f9e9cf962051a5f6aab37d2bcfd3d6d8e9322362a6f592aa72562673ff06ef6ac3d1cc101d957cb', '28f0173123cf4727db96ab681500596a9c6d006a316c2ba55996f30d6623a3589368c8dddb3356b5', 0, '2022-04-19 02:22:40'),
('3fcd240be7b467bd9d004967646a4132207e2537cbd08798a62bbf26ac82a62a540c357a39ca1dff', '5555fed37e07118e204b31eeea29b4b440fc15de1f30cb92bb00f750af214f96d807e8ea2ca54ae6', 0, '2022-04-19 02:18:11'),
('41af882a3826eb65c6ef9ec3041699bd997d7db1bae375417a31fa151147c2e0416791b8d153624e', '91862c2522b429336320ebbc05b29459d10417782f62bb9dab5d9c7b088bd3680cfe8ea6fd290310', 0, '2022-04-19 02:16:30'),
('468c903efd47fa9f978cae0969c26d407579a5751fb7c8e77e5d1636fb4063dbe9a7aeaf6ae46b54', '27351c3fb37a19c1574c9c5dc08b794a1a83e0ac60617f00b1d3d712cc2af45ad2a680d48ff05ecc', 0, '2022-04-19 01:49:02'),
('46aa7c65bc7bd83bcf4812e343a8800e96bdd6b17d9978e29121c2b60cf75672d8baeeb0c365e9d3', '58c730ec046a671786203a96102948bcf2df53fd88c4098ba5c39dd9985ac06d2b11e4e088dcd3db', 0, '2022-04-26 12:01:18'),
('4a425d04e7c7a66af88ad585396645b412ecc9ed910bdda83a6b0be9f00b8772d50e5a4d4c67eb36', 'a0558e1d91c375464e9ec183ca714ca4527357a78f34b58f353bac4eeabc5dda1d93be908134c530', 0, '2022-04-19 01:55:51'),
('520d16debe1a09605a9487428b0d6ef73417ae04bfe94c8a81a37687923d0b4ded55338901d94d43', 'a1e0bb2c8c4043d54bff95c3d98321dc888de6717e5b0f8c595aa68fe622dc19d91187357c0bd285', 0, '2022-04-19 02:15:33'),
('56e4ae6da9bab663d6fb8834de418fc230a28571cb41d87f6d9eba536d324664a984bffa808c0411', '64bd97c57652190ec4603c56f1023f6c11278ae4a111e8a2b5ddd8893583d226b60c77784c6b4077', 0, '2022-04-20 05:28:44'),
('58558f82ce3a58bcdfa52703edd7367e39421e8b50e2bd5f3f4f2287f7a65f282e6106e69f0cf782', 'ed801ba930b148065b1fe58720b241ed97e3a93fa9f03f83b27d9a44677975f8b3571338e706a808', 0, '2022-04-19 01:48:34'),
('5b93d4c2cac64d3b2dfde757cd9a334d92bd0557194815864306ba29b56adcd99edd776608ae71ea', '37592d98c809a1735f90ae998c18db2bf9e38efa224a56639b19fe5ddc5c92c9019dbfdb8097d18b', 0, '2022-04-19 02:17:51'),
('5c22b220f1393e1b2c0aeeb98e817f98367d7609dcee4e3952910992619a27888b63b88d28e6509b', 'cc5ccb0c99f83ab373e5e0460dd80fe580d7415e222c2db348df7be6d144200fd53c03a0e1d72348', 0, '2022-04-19 02:06:10'),
('67f8fe25417b07c4d30c9677a5af1678bffa2c803792285495d8d1a63d0dfe5be62dc93f85d4c766', '26b98c575e0d1e19329e8485d03fbdb3be3d7e3e6c74c96a63ca7beca65df5cc2d7f0bd269ff0c8f', 0, '2022-04-19 02:19:19'),
('7027913fe1aa5d7f0a9a8b80653af9bef524768f0c3ccdd758325cb023bd5db7cc4dadff3f3962f5', '0fa5e8c5b6821a832d287f8fe4c3184c874f73a4662ceed09483ddb9dc4f6f016938344adda3042c', 0, '2022-04-19 02:17:12'),
('70d88c3c06e7ca5c2f3c257e07a5238e7706f14b5d73c6c080405c666b2df708eb646d88a01582ab', '53f0832f2d3b18a95fa741b83e96ef113d5686124b0a4c01c0488fef60b966ec04c87cf59eb9b399', 0, '2022-04-19 02:17:27'),
('766dcb5f868964dacfbc24ba834436852a57ac3c818e8de9883a6bf280f76f837f586e929f100634', '6a7ed9f26759bfe06ae24084a15f2893d77026f2cd6f61b2c2a207f8d77e449ccc654dcedf5ea4b5', 0, '2022-04-19 01:50:03'),
('7950483585b45924383bf644ca6c86e3e0da17e9e4b519b634aecf8eaa88f7994f73506265ef685e', '84ef85755ce65759d001a1feff869d4eaa713e2ab8f04abd29751f36023d137c7b87ec7dde1840d3', 0, '2022-04-19 01:54:27'),
('7fbbe15d0a3a4747d80a81f2ab37f8ea52d234c17e0f4eb3a4a88d40240f9575e8abd29b547f0ccb', 'cf56928dacb8424093bfe53457a8b6b1542259408470bb67a1cdf2ef1a76ad9a8b0baa613e8913cf', 0, '2022-04-19 01:56:55'),
('809a5a9b074b68d9a38b0345c25076b158a8f72db8144417f8a33f245066bddddd18193dfffdce9d', '6c9428a359645b855e17e4e87f7f14b6837ae18cee40aacbd37123262ae907be829558a473b2df1c', 0, '2022-04-20 00:34:31'),
('855840ce5d77cbac7d7b0125d6fe3267404bc52ad00253e5706d3605d66994bc77a676970e9f6fd8', 'bd755e15f1ec93b79e561d171955dd867fab5c612f2c868fef433c8276329ec2118e79105a9d6c7e', 0, '2022-04-19 01:56:05'),
('8cb3cfcffa4a0a8fd040844c450ae934804ca98c41db0b53daf6b64e4156dd11dccbb629c377886a', 'e5d6cdea4ee293ddcf77b72b5477e609900fe5f548d23f061d304b3961b10a5879e348135595beaa', 0, '2022-04-19 01:58:32'),
('8d54db913a2cd3e604640ad9c52119dd727be50370cface0d7c19954542d3f114973a8745c5db13b', '9fff008d3ef077427b1e2602e2604176c5e3ae5b2ae5ccecf4623ed82e9f65399a5d237980d1d9fa', 0, '2022-04-19 01:57:37'),
('9767594c81e0763271b693017da053cca5b7dbb4aef6a1304741805f2fb962d2406b414cf7cf76a3', 'ce52795cbe991bb654e19985e7fc2a04b38c35920e9ea261a04212c981b229c2689a34fcd1993fa5', 0, '2022-04-19 01:59:17'),
('9b436aa91bce4b0b188915c67a4cd59113d11ae2b5f749f2e6c1b550764a9ba0febd65856477adc2', '7729f561fd3eb595c33fcf67b437723c22649eb1ec08be393f4f802112ac463ef9434ff2cac0b450', 0, '2022-04-19 02:14:30'),
('9e8003ab88033489ccf1eeaf6b4db7de0240fc0e83b284d866f131d85965322dfeadcac0fd01c51f', '8fb794a34d4171574d9c6b1fbae8eade694d4ef892d2d9704d11d30447ff848a144acbbd879fe91b', 0, '2022-04-19 01:49:36'),
('9fe90405902b1c0f44b3de089698d6ee32d793dd46353194f02e99a7f418d9b48d2510c42070238b', 'c8d5328f740f72ccdd57f011654ad83c4f911d16546780f0e644a7ed9afaef808085808698f0b34a', 0, '2022-04-19 02:00:45'),
('ac33093049fbd3f51541c14c4dd438525799cca70219fad6f3466101457f4f748e9753f6c0d34834', '97dfc0c36bb7a8e3525b06cc245885051064962e9dd7aa12649311559af95aba78861fbd036a83f5', 0, '2022-04-19 01:50:54'),
('b47ffcba9bce07fbecb97a3ff76711e063efbe511053a4da96eb3f5aff5dc0fb385aa08fede1f037', '7a9b70310331074be4bd07c53bc9865cde2fff258ce2570b785c9e1259665d8033b03508b2355f08', 0, '2022-04-19 02:15:00'),
('b778025bc6839936942016d32d30795485904536a8aba66edc14ac7050b55ad989e0758558f429be', '7cbcfd1913a85db78462fa76fee99d476d0474cb89f996434598fa621d61ceb133cce3a761b44170', 0, '2022-04-19 02:02:58'),
('bb70023b8222933570eaf8f39eed63fe4ad0ceb5c63fdf8e69b0e405de697f23b7c0a95844aa6819', 'f4d241a1da48b2c7b6f495f96304ca14bd19839519b7548c20492b90e1aaf4cf5d64f3ea0265d66f', 0, '2022-04-20 05:11:40'),
('bb9d9397e5fb6429c19448ce56eedf5e436e06c6b788d00475b2bf0c87203384254f1d626dd987be', 'a098f4ed28d59741c32dcf47eb5774a8168d935e051ac303a18d2543bbea97046f684f5361417afe', 0, '2022-04-19 01:54:03'),
('c3dbecf4a8077575e731b42b1edfa766e80322fe828378f12639d87b188c94c382dd57043e0854a0', 'e8d5344da7a2598c662412a41055b9cdf0e340184e6476aedc53ba35b641a25a22739c3259c314f7', 0, '2022-04-19 02:15:15'),
('c877a08c0c7ac28b9a05eda36130e279c503b892622f15e645a0882f89927415787f794ec7aba43f', '16f681119f15f75aa19101bbc489b7c3322c8e77bec646de4465f9970498a5326e66bd51978bb988', 0, '2022-04-19 02:00:56'),
('c9a248429aa348a46a9cde42306869275c54241493727ddb4dbb6f2d2e7d35748eb4fccfe885a982', 'df9bcf1bb3928967282ee889c9c9744ffb9f476af46f9010622217f16696791f398962bd47ce16cd', 0, '2022-04-19 01:52:38'),
('cf04cd53e8b1d562ae68bd3e5319c5707744ac7172af440515f54ed86cd5901f715d0215e3f4b5c7', '8599ce819a5e66d58e4b2a760770507c501e7567931015fab90a7400531d02cf59b35262a6222272', 0, '2022-04-19 02:28:52'),
('d5e4f24ef648d705ea975b32f9cb65fa382022b18426b7dd6953f8cd46e847ba53321210dbefbe85', '58b8a8b462e1bc3f46fbdd42b132e61217f4d49a1c329d94a0c7de19017bdbbbae7d140531c370d0', 0, '2022-04-19 01:57:15'),
('d8ca77805cb6857e1ad8977683d82f993f65485f211a3a119fb11e84ccb3917670c5bbf678ab0b44', 'b617e5ff7f70c6a3a73a9e9fb7d3afe4a579e2ee9e29a50b93355921de6a7c24c41ac7d7c5e348bc', 0, '2022-04-19 02:04:53'),
('da5e76b1e9c4c112b51f2c9424c4f3fa80e42e88a70ab0699ea1d021e1a278272c3d22cadcdfc546', 'e01cc84e3e43acd2157ac3d6254baa8c8ef96b073a417599e01a0b2272f42f11ffd53efa027dca27', 0, '2022-04-19 02:12:59'),
('dacb9891a7701d5e69f71befa1b71f565ceca861bbe7c5066de81a7dcf7efc7b97a61f4d456cf0c6', '475367e7f14166a0c74c171fafefe236c488e058a7235d3808980b5773b2e9e28cb690478d28244f', 0, '2022-04-19 01:51:11'),
('e25fa927a7e05ce45d4d8759a6943874102a3bd745d98b56f8f065e93085d052fb5c96d15e5b47c6', '343ba4b9543fb733cd7864e7b6623a56970e60e6af9f2e9e8461d56e53576d13c9cd37587a03d203', 0, '2022-04-19 01:50:17'),
('e36d950352801e7c82922022996399a6ff930b59f38239c11f88d8149891fb58b016fddffb4e408a', '38064bd8a191ccae9b20822c3bd80f3782e6c3bfb1463a0365f23586e45b275fb1772f2b3f9283f9', 0, '2022-04-19 02:15:42'),
('e3d204ba82340214d41f2444c14f4104b3f3886f8c9135921fa4db78d15a0d058de8254261e86350', 'f32150baaffd21a0215710ed0f61a53eabf1f01aac7c594979efb5a6a1028f86b2f635250870efff', 0, '2022-04-19 02:18:28'),
('e47e266fcfb21c773709d26e2c900c2bede085bd52a8c88c080bbe29c1222a36489da4ca7dd09459', '1a9ba8f519c48bb15828c553094b8fc6e8b98c7b6f7324c3b4e3a0182145282c2ee929ecd1670117', 0, '2022-04-19 01:57:26'),
('f4011f6cbb0f10d343f02614aa19d824e7b85864310e0cf7e3c589eaaba439fee3740234871ae285', 'baf9fdfabc7e262d862a81c0048320d847b01b379fee9c7d80dcab8813c0f410b7460dae3fcbc171', 0, '2022-04-19 01:59:00'),
('fb8b493d4c5655e595a2a082b320f5b630253ac6dfc719daedf5dec303549ab866c701c047c24680', '9e71051a8f24dcf587b924b94e431f642288132d053881701d53055718b7a0044a3647d967548120', 0, '2022-04-23 08:11:11'),
('fc30963f806597ac4f3f03f8879abc347c9a1267a93ac6b0e98f3b64bec72514abe964568614f7c2', 'eb1448a3a4928637ade8409c61cdda04192143cbf728a821e1cc706f94ba8de5a334820ee3b98f38', 0, '2022-04-19 02:01:06'),
('fdbbc26526f375eaf66981cb8892f23e29413cbc56abe51ca1f32a0adc7d320d1ae598e0eea1f87b', 'c7834835665a63f4fef4deb060acc711c37d4e5506ab627f37e9f322449ee75f2dc07b3c8e5137ac', 0, '2022-04-20 00:50:43'),
('ff65faf06ca1c908b3d1a4aba654bb20ccd5e4c5cccbe55c8639a93552f9a4408b1b12132f79fa80', '669ba7fcf5e9d9e9214e05b6f02e5b114c433a95d6d08986fb6123532887d3e9080503b807c67893', 0, '2022-04-19 02:13:30');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Admin', NULL, NULL, NULL),
(2, 'service-supervisor', 'Service Supervisor', NULL, NULL, NULL),
(3, 'staff-manager', 'Staff Manager', NULL, NULL, NULL),
(4, 'payroll-officer', 'Payroll Officer', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(1, 1),
(2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `address`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', '12 Normanby rd', 'admin@admin.com', NULL, '$2y$12$hUpAPCIelN1/ss.j8IbYNOUDuEgjfQiFidzaj72J4ROR4gnU3rewS', NULL, NULL, NULL),
(2, 'John Adam', '11 Hornsby rd', 'johnadam@gmail.com', NULL, '$2y$12$cHY47WK3Umo2wEoZLg/kNO0WfjW9vW8jN7wpYSXwM4rRZ.5BZCT2S', NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_client_id_foreign` (`client_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `jobs`
--
ALTER TABLE `jobs`
  ADD CONSTRAINT `jobs_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
