<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesAvaibilitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees_avaibilities', function (Blueprint $table) {
            $table->id();
            $table->foreignId('employee_id')
                ->constrained('employees','id');
            $table->text("employee_hour");
            $table->date('availability_for_start_date');
            $table->date('availability_for_end_date');
            $table->tinyInteger('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees__avaibilities');
    }
}
