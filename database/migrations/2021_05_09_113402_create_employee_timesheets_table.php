<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeTimesheetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_timesheets', function (Blueprint $table) {
            $table->id();
            $table->foreignId('employee_id')
                ->constrained('employees','id');
            $table->foreignId('job_id')
                ->constrained('jobs','id');
            $table->dateTime('start_date_time')->nullable();
            $table->dateTime("end_date_time")->nullable();
            $table->tinyInteger('is_special_shift')->default('1');
            $table->string('special_rate')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_timesheets');
    }
}
