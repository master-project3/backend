<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('description');
            $table->dateTime('start_date');
            $table->dateTime('end_date');
            $table->tinyInteger('is_full_day_event')->default(0);
            $table->dateTime('recurring_end_date')->nullable();
            $table->tinyInteger('is_recurring');
            $table->string('created_by');
            $table->text('location');
            $table->text('note');
            $table->tinyInteger('status');
            $table->integer('duration')->nullable();
            $table->text('rrule');
            $table->foreignId('client_id')
                ->constrained('clients','id')
                ->onUpdate('cascade');
            $table->integer('no_of_employee')->nullable();
            $table->string('paid_frequency')->default('weekly');
            $table->text('base_rate');

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
