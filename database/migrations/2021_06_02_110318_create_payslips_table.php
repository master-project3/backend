<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePayslipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payslips', function (Blueprint $table) {
            $table->id();
            $table->foreignId('job_id')
                ->constrained('jobs','id');
            $table->foreignId('employee_id')
                ->constrained('employees','id');
            $table->enum("status",['pending','paid']);
            $table->text('description');
            $table->integer("amount");
            $table->date('period_for_start_date');
            $table->date('period_for_end_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payslips');
    }
}
