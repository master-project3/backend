<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            $table->string("name");
            $table->string('phone_number');
            $table->text('skill');
            $table->string('email')->unique();
            $table->text("description");
            $table->string('address');
            $table->string('base_rate');
            $table->string('frequency');
            $table->string('photo');
            $table->string('license_number');
            $table->tinyInteger('status');
            $table->string('bsb_number');
            $table->string('account_number');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
