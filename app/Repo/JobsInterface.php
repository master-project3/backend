<?php


namespace App\Repo;


Interface JobsInterface
{
    public function getSpecificJobs( $Jobs_id, $show_child);

    public function getAllJobs($client_id,$start_date, $end_date);

    public function createJobs(array $Jobs);

    public function updateJobs( $Jobs_id, array $Jobs);

    public function deleteJobs($Jobs_id);
}
