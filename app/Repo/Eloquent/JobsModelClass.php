<?php


namespace App\Repo\Eloquent;


use App\Models\Jobs;
use App\Repo\JobsInterface;

class JobsModelClass implements JobsInterface
{
    private $job;
    public function __construct(Jobs $job)
    {
        $this->job = $job;
    }
    public function getSpecificJobs($jobs_id, $show_child = false)
    {
        return $this->job->findOrFail($jobs_id);
    }

    public function listAllJobsWithoutClient(){
        $jobs = $this->job->orderBY('client_id','desc')->get();
        foreach ($jobs as $job){
            $client = $job->client()->get();
            $job['client_name'] = $client[0]['name'];
        }
        return $jobs;
    }

    public function getAllJobs($client_id, $start_date, $end_date)
    {
        return $this->job->where('client_id',$client_id)
            ->where(function ($q) use ($start_date,$end_date){
                $q->whereBetween('start_date',[$start_date,$end_date])
                    ->orwhereBetween('recurring_end_date' ,[$start_date,$end_date])
                    ->orwhereBetween('end_date' ,[$start_date,$end_date])
                    ->orWhere(function ($q) use ($start_date,$end_date){
                        $q->where('start_date','<=',$start_date)->where('recurring_end_date' ,'>=',$end_date);
                    });
            })
            ->orderBY('start_date','asc')
            ->get();
    }

    public function createJobs(array $job)
    {
        return $this->job->create($job);
    }

    public function updateJobs($jobs_id, array $job)
    {
        $this->job->findOrFail($jobs_id)->update($job);
        return true;
    }

    public function deleteJobs($job_id)
    {
        return $this->event->findOrFail($job_id)->delete();
    }

}
