<?php

namespace App\Providers;

use App\CalendarClass\CalendarInterface\EventClassInterface;
use App\CalendarClass\EventClass;
use App\JobsClass\JobsClass;
use App\JobsClass\JobsInterface;
use App\Repo\Eloquent\JobsModelClass;
use Illuminate\Support\ServiceProvider;
use Recurr\Rule;
use Recurr\Transformer\TextTransformer;
use Validator;
use Laravel\Passport\Passport;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(\App\Repo\JobsInterface::class,JobsModelClass::class);
        // app/JobsClass
        $this->app->bind(JobsInterface::class,JobsClass::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Passport::routes();
        Passport::tokensCan([
            'admin' => '',
            'service-supervisor' => '',
            'staff-manager' => '',
            'payroll-officer' => ''
        ]);

        Validator::extend('abn_validation', function($attribute, $value, $parameters, $validator) {
            $number = preg_replace('/[^0-9]/', '', $value);
            if(strlen($number) != 11) {
               return false;
            }

            $weights = array(10, 1, 3, 5, 7, 9, 11, 13, 15, 17, 19);

            // Strip non-numbers from the acn
            $abn = $number;

            // Subtract one from first digit
            $abn[0] = ((int)$abn[0] - 1);

            // Sum the products
            $sum = 0;
            foreach(str_split($abn) as $key => $digit) {
                $sum += ($digit * $weights[$key]);
            }

            if(($sum % 89) != 0) {
                return false;
            }

            return true;

        });

        Validator::extend('week_day_validation', function($attribute, $value, $parameters, $validator) {
            $test=in_array($value,["sunday","monday","tuesday","wednesday","thursday","friday","saturday"]);
            if(!$test){
                return false;
            }
            return true;
        });



        Validator::extend('validate_rrule', function($attribute, $value, $parameters, $validator) {
            try{
                $total_rrule = explode('RRULE:',$value);
                $count = count($total_rrule);
                if($count == 2){
                    $rrule = $total_rrule[1];
                }
                if($count == 1){
                    $rrule = $total_rrule[0];
                }
                if($count > 2){
                    return false;
                }
                $rule = new Rule($rrule, new \DateTime());


                return true;
            }
            catch (\Exception $exception){
                return false;
            }


        });


    }
}
