<?php

namespace App\Http\Controllers;

use App\JobsClass\JobsClass;
use App\Models\Jobs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\ValidationException;
use Recurr\Rule;

class JobsController extends Controller
{
    private $jobs;
    public function __construct(JobsClass $jobs)
    {
        $this->jobs = $jobs;
    }


    public function getSpecificJob($id){
        return $this->jobs->getSpecificJobs($id);
    }

    public function createJobs(Request $request){
        DB::beginTransaction();
        try{
            $message = [
                'reminder.*.format.in' => 'The reminder format must be either minutes,hours,days or weeks.',
                'reminder.*.notification_type.in' => 'The notification type must be either email or notification.',
                'recurring_type.in' => 'The :attribute must be either daily,weekly,monthly or yearly.',
                'visibility' => 'The :attribute must be either free or busy.',
                'privacy' => 'The :attribute must be either default,private,public.',
                'rsvp' => 'The :attribute must be either yes,no or maybe.',
                'paid_frequency.in' => 'The :attribute must be either weekly,fortnightly,monthly,yearly',
                'rrule.validate_rrule' => "The :attribute is not a valid RRULE format."

            ];
            $this->validate($request,[
                'title' => 'required|string',
                'description' => 'required|string',
                'is_full_day_event' => 'required|boolean',
                'start_date' => 'required|date_format:Y-m-d H:i:s',
//                'end_date' => 'required|date_format:Y-m-d H:i:s|after:start_date',
                'category_id' => 'required|exists:category,id',

                'is_recurring' => 'required|boolean',
                'recurring_end_date' => 'Y-m-d H:i:s|after:start_date',
//                'created_by' => 'required|email|string',
                'location' => 'required|string',
                'note' => 'string',
                'status' => 'required|boolean',
                'is_accepted' => 'required|boolean',
                //recurring request
                "recurring_type" => "required_if:is_recurring,1|in:yearly,monthly,weekly,daily",
//                "recurring_by_month" => "required_if:recurring_type,yearly|integer|min:1|max:12",
//                "recurring_by_month_day" => "required_if:recurring_type,yearly|integer|min:1|max:32",
//                "recurring_by_day" => "required_if:recurring_type,weekly|array|required_with:recurring_by_set_pos",
//                "recurring_by_day.*" => "integer|min:0|max:6",
//                "recurring_by_set_pos" => "integer|min:1",
//                "recurring_by_until" => "date_format:Y-m-d",
//                "recurring_count" => "integer|min:1",
//                "recurring_interval" => "required_if:recurring_type,monthly,weekly,daily|integer|min:1",
            'rrule' => 'required_if:is_recurring,1|string|validate_rrule',
                'duration' => 'integer',
                'client_id' => 'required|exists:clients,id',
                'no_of_employee' => 'required|min:0|integer',
                'paid_frequency' => 'required|in:weekly,monthly,fortnightly,yearly',
                'base_rate' => 'required|min:100|integer|max:999999'

            ],$message);
        }
        catch (ValidationException $ex){
            return response()->json([
                'status' => '422',
                'message' => $ex->errors()
            ],422);
        }

        try{
        //get logged in user info
        $loggedInUser = Auth::user();
        $email = $loggedInUser->email;
        $request->merge([
            'created_by' => $email
        ]);


        if($request->is_recurring == 0){
            $request->merge([
                'rrule' => ''
            ]);

        }
        if(!$request->has('note')) $request->merge(['note' => '']);
        $this->jobs->createJobs($request->except(
            [
                'recurring_type','recurring_by_month','recurring_by_month_day',
                'recurring_by_set_pos','recurring_by_until','recurring_count',
                'recurring_interval','recurring_by_day'
            ]));
        DB::commit();
       return response()->json([
           'status' => 200,
           'message' => 'Job created successfully'
       ]);


        }
        catch (\Exception $exception){
            return response()->json([
                'status' => 500,
                'message' => $exception->getMessage()
            ],500);
        }

    }

    public function updateJobs($id,Request $request){
        DB::beginTransaction();

        try{
            $message = [
                'reminder.*.format.in' => 'The reminder format must be either minutes,hours,days or weeks.',
                'reminder.*.notification_type.in' => 'The notification type must be either email or notification.',
                'recurring_type.in' => 'The :attribute must be either daily,weekly,monthly or yearly.',
                'visibility' => 'The :attribute must be either free or busy.',
                'privacy' => 'The :attribute must be either default,private,public.',
                'rsvp' => 'The :attribute must be either yes,no or maybe.',
                'paid_frequency.in' => 'The :attribute must be either weekly,fortnightly,monthly,yearly',
                'rrule.validate_rrule' => "The :attribute is not a valid RRULE format."

            ];
            if($request->has('change_job_acceptance') && $request->change_job_acceptance){
                $this->validate($request,[
                    'is_accepted' => 'required|boolean'
                ]);
            }
            else{
                $this->validate($request,[
                    'title' => 'required|string',
                    'description' => 'required|string',
                    'is_full_day_event' => 'required|boolean',
                    'start_date' => 'required|date_format:Y-m-d H:i:s',
                    'category_id' => 'required|exists:category,id',
//                'end_date' => 'required|date_format:Y-m-d H:i:s|after:start_date',


                    'is_recurring' => 'required|boolean',
                    'recurring_end_date' => 'Y-m-d H:i:s|after:start_date',
//                'created_by' => 'required|email|string',
                    'location' => 'required|string',
                    'note' => 'string',
                    'status' => 'required|boolean',
                    'is_accepted' => 'required|boolean',
                    //recurring request
                    "recurring_type" => "required_if:is_recurring,1|in:yearly,monthly,weekly,daily",
//                "recurring_by_month" => "required_if:recurring_type,yearly|integer|min:1|max:12",
//                "recurring_by_month_day" => "required_if:recurring_type,yearly|integer|min:1|max:32",
//                "recurring_by_day" => "required_if:recurring_type,weekly|array|required_with:recurring_by_set_pos",
//                "recurring_by_day.*" => "integer|min:0|max:6",
//                "recurring_by_set_pos" => "integer|min:1",
//                "recurring_by_until" => "date_format:Y-m-d",
//                "recurring_count" => "integer|min:1",
//                "recurring_interval" => "required_if:recurring_type,monthly,weekly,daily|integer|min:1",
                    'rrule' => 'required_if:is_recurring,1|string|validate_rrule',
                    'duration' => 'integer',
                    'client_id' => 'required|exists:clients,id',
                    'no_of_employee' => 'required|min:0|integer',
                    'paid_frequency' => 'required|in:weekly,monthly,fortnightly,yearly',
                    'base_rate' => 'required|min:100|integer|max:999999'

                ],$message);
            }

        }
        catch (ValidationException $ex){
            return response()->json([
                'status' => '422',
                'message' => $ex->errors()
            ],422);
        }

        try{
            if($request->has('change_job_acceptance')  && $request->change_job_acceptance){
               $job = Jobs::findOrFail($id);
               $name = $job['title'];
               $client = $job->client->toArray();
              $client['job_name'] = $name;
                $this->jobs->editJobs($id,$request->only(
                    [
                        'is_accepted'
                    ]));
                if($request->is_accepted){
                    $client['job_status'] = "Accepted";
                    $messages = "Job is accepted";
                }
                else{
                    $client['job_status'] = "Rejected";
                    $messages = "Job is rejected";
                }

                Mail::send('mail.job-approved', $client, function($message) use ($client) {
                    $message->to($client['email'], $client['name'])->subject
                    ("Job is" );
                    $message->from('no-reply@jobfitts.com','Jobfitts Consultant');
                });
                DB::commit();
                return response()->json([
                    'status' => 200,
                    'message' => $messages
                ]);
            }

            //get logged in user info
            $loggedInUser = Auth::user();
            $email = $loggedInUser->email;
            $request->merge([
                'created_by' => $email
            ]);


            if($request->is_recurring == 0){
                $request->merge([
                    'rrule' => ''
                ]);

            }
            if(!$request->has('note')) $request->merge(['note' => '']);
            $this->jobs->editJobs($id,$request->except(
                [
                    'recurring_type','recurring_by_month','recurring_by_month_day',
                    'recurring_by_set_pos','recurring_by_until','recurring_count',
                    'recurring_interval','recurring_by_day'
                ]));
            DB::commit();
            return response()->json([
                'status' => 200,
                'message' => 'Job updated successfully'
            ]);


        }
        catch (\Exception $exception){
            return response()->json([
                'status' => 500,
                'message' => $exception->getMessage()
            ],500);
        }

    }

    public function listAllJobs(){
        return response()->json([
            'status' => 200,
            'payload' => $this->jobs->listAllJobs()
        ]);
    }

    //returns the recurring pattern into ical rrule
    private function getRrule($request){
        $day = [];
        if($request->has('recurring_by_day')){
            $remove_days_repetition = array_unique($request['recurring_by_day']);
            foreach ($remove_days_repetition as $day_rep){
                switch ($day_rep){
                    case 0 :
                        $day[] = 'SU';
                        break;
                    case 1:
                        $day[] = 'MO';break;
                    case 2:
                        $day[] = 'TU'; break;
                    case 3:
                        $day[] = 'WE'; break;
                    case 4:
                        $day[] = 'TH';break;
                    case 5:
                        $day[] = 'FR'; break;
                    case 6:
                        $day[] = 'SA'; break;
                }
            }
        }
        $rule = new Rule();
        $r_rule = $rule->setFreq(strtoupper($request['recurring_type']));
        //The maximum field for ical rrule in case of yearly are by_month,by_monthday,by_day,by_set_pos,until,count
        if($request->recurring_type == 'yearly'){
            if($request->has('recurring_by_month') && $request->filled('recurring_by_month')){
                $r_rule->setByMonth([$request->recurring_by_month]);
            }
            if($request->has('recurring_by_month_day') && $request->filled('recurring_by_month_day') ){

                $r_rule->setByMonthDay([$request->recurring_by_month_day]);
            }
            if($request->has('recurring_by_day')){
                $r_rule->setByDay($day);
            }
            if($request->has('recurring_by_set_pos') && $request->filled('recurring_by_set_pos')  ){
                $r_rule->setBySetPosition([$request->recurring_by_set_pos]);
            }
            if($request->has('recurring_by_until') && $request->has('recurring_count')){
                $r_rule->setUntil(new \DateTime($request->recurring_by_until));
            }
            else{
                if($request->has('recurring_by_until') &&  $request->filled('recurring_by_until')){
                    $r_rule->setUntil(new \DateTime($request->recurring_by_until));
                }
                if($request->has('recurring_count') && $request->filled('recurring_count')){
                    $r_rule->setCount($request->recurring_count);
                }
            }
            if($request->has('recurring_interval') && $request->filled('recurring_interval')){
                $r_rule->setInterval($request->recurring_interval);
            }

        }
        elseif ($request->recurring_type == 'monthly'){
            if($request->has('recurring_by_day')){
                $r_rule->setByDay($day);
            }
            if($request->has('recurring_by_set_pos') && $request->filled('recurring_by_set_pos')  ){
                $r_rule->setBySetPosition([$request->recurring_by_set_pos]);
            }
            if($request->has('recurring_by_until') && $request->has('recurring_count')){
                $r_rule->setUntil(new \DateTime($request->recurring_by_until));
            }
            else{
                if($request->has('recurring_by_until') &&  $request->filled('recurring_by_until')){
                    $r_rule->setUntil(new \DateTime($request->recurring_by_until));
                }
                if($request->has('recurring_count') && $request->filled('recurring_count')){
                    $r_rule->setCount($request->recurring_count);
                }
            }

            if($request->has('recurring_by_month_day') && $request->filled('recurring_by_month_day')){
                $r_rule->setByMonthDay([$request->recurring_by_month_day]);
            }
            if($request->has('recurring_interval') && $request->filled('recurring_interval')){
                $r_rule->setInterval($request->recurring_interval);
            }
        }
        elseif ($request->recurring_type == 'weekly'){
            if($request->has('recurring_by_day')){
                $r_rule->setByDay($day);
            }
            if($request->has('recurring_by_until') && $request->has('recurring_count')){
                $r_rule->setUntil(new \DateTime($request->recurring_by_until));
            }
            else{
                if($request->has('recurring_by_until') &&  $request->filled('recurring_by_until')){
                    $r_rule->setUntil(new \DateTime($request->recurring_by_until));
                }
                if($request->has('recurring_count') && $request->filled('recurring_count')){
                    $r_rule->setCount($request->recurring_count);
                }
            }
            if($request->has('recurring_interval') && $request->filled('recurring_interval')){
                $r_rule->setInterval($request->recurring_interval);
            }
        }
        else{
            if($request->has('recurring_by_until') && $request->has('recurring_count')){
                $r_rule->setUntil(new \DateTime($request->recurring_by_until));
            }
            else{
                if($request->has('recurring_by_until') &&  $request->filled('recurring_by_until')){
                    $r_rule->setUntil(new \DateTime($request->recurring_by_until));
                }
                if($request->has('recurring_count') && $request->filled('recurring_count')){
                    $r_rule->setCount($request->recurring_count);
                }
            }
            if($request->has('recurring_interval') && $request->filled('recurring_interval')){
                $r_rule->setInterval($request->recurring_interval);
            }
        }


        return $r_rule->getString();


    }
}
