<?php

namespace App\Http\Controllers;

use App\Models\Clients;
use App\Models\Employees;
use App\Models\Invoice;
use App\Models\Jobs;
use App\Models\Payslips;
use http\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function getReport(){
        $report['account_report'] = $this->getAccountReport();
        $report['client_report'] = $this->getClientStatus();
        $report['job_report'] = $this->getJobStatus();
        $report['daily_report']=$this->getInvoiceTimeline();
        return response()->json([
            'status' => 200,
            'payload' => $report
        ]);
    }

    private function getAccountReport(){
        $client = new \GuzzleHttp\Client();
        $credentials = base64_encode("e91cba03-507a-458a-9119-7663fcc7abe7:''");
        $request = $client->get('https://api.m-pay.com.au/mAccount/v1/financials/6279059733330515',["headers" => [
            "Authorization" => "Basic " . $credentials
        ]]);
        $response = json_decode((string)$request->getBody(), true);
        $totalReceived = Invoice::where('status','paid')->sum('amount');
        $totalPaid = Payslips::where('status','paid')->sum('amount');
        $totalUnreceived = Invoice::where('status','pending')->sum('amount');
        $totalUnpaid = Payslips::where('status','pending')->sum('amount');
       return [
           "balance" => $response['financials']['actualBalance'],
           'total_received_from_client'=> $totalReceived,
           'total_pending_receive_client' => $totalUnreceived,
           'total_paid_to_employee' => $totalPaid,
           'total_unpaid_employee' =>$totalUnpaid
       ];

    }

    private function getClientStatus(){
        $total_client = Clients::count('*');
        $total_client_approved_jobs = Jobs::where('is_accepted',1)->distinct('client_id')->count('client_id');
      return [
        'total_clients' => $total_client,
        'total_client_approved_jobs' => $total_client_approved_jobs
      ];

    }

    private function getJobStatus(){

      $total_accepted_jobs = Jobs::where('is_accepted',1)->count('*');
        $total_pending_jobs = Jobs::where('is_accepted',0)->count('*');
        $total_assigned_jobs = DB::table('employee_jobs')->distinct('job_id')->count('job_id');;
        $total_unassigned_job = $total_accepted_jobs- $total_assigned_jobs;
        $total_employee = Employees::count('id');
        $total_assigned_employee = DB::table('employee_jobs')->distinct('employee_id')->count('employee_id');;
       $total_unassigned_employee =  $total_employee - $total_assigned_employee;
       return [
           'total_jobs' => $total_accepted_jobs + $total_pending_jobs,
           'total_accepted_jobs' => $total_accepted_jobs,
           'total_pending_jobs' => $total_pending_jobs,
           'total_assigned_jobs' => $total_assigned_jobs,
           'total_unassigned_job' => $total_unassigned_job,
           'total_employee' => $total_employee,
           'total_assigned_employee_to_job' => $total_assigned_employee,
           'total_unassigned_employee' => $total_unassigned_employee
       ];
    }

    private function getAccountStatus(){

    }

    private function monthlyIncomeAndExpenditure(){

    }

    private function getInvoiceTimeline(){
//        $invoice_paid_history = DB::table("invoices")->raw('SELECT DATE(updated_at) AS date,
//                                IFNULL(SUM(amount),0) AS total_received
//                           FROM invoices
//                          WHERE (updated_at BETWEEN (SELECT MIN(DATE(updated_at)) FROM invoices) AND (SELECT MAX(DATE(updated_at)) FROM invoices)) and status ="paid"
//                          GROUP BY date')->get();
        $invoice_paid_history = DB::select( DB::raw('SELECT DATE(updated_at) AS date,
                                IFNULL(SUM(amount),0) AS total_received
                           FROM invoices
                          WHERE (DATE(updated_at) BETWEEN (SELECT MIN(DATE(updated_at)) FROM invoices) AND (SELECT MAX(DATE(updated_at)) FROM invoices)) and status ="paid"
                          GROUP BY date'));
        $invoice_paid_history = array_map(function ($value) {
            return (array)$value;
        }, $invoice_paid_history);

        $payslip_paid_history =DB::select( DB::raw('SELECT DATE(updated_at) AS date,
                                IFNULL(SUM(amount),0) AS total_paid
                           FROM payslips
                          WHERE (DATE(updated_at) BETWEEN (SELECT MIN(DATE(updated_at)) FROM payslips) AND (SELECT MAX(DATE(updated_at)) FROM payslips)) and status ="paid"
                          GROUP BY date'));

        $payslip_paid_history = array_map(function ($value) {
            return (array)$value;
        }, $payslip_paid_history);
        return [
            'daily_invoice_amount_received' => $invoice_paid_history,
            'daily_money_paid_to_employee' => $payslip_paid_history
        ];
    }

    public function getTransactionHistory(){

        $param = [
               "accountNumber"=> "6279059733330515",
               "frequency"=> "Custom",
               "startDate"=> "2021-05-05T00:00:00",
               "endDate"=> "2021-06-30T00:00:00",
               "descending"=> true,
               "useTime"=> true
             ];
        $client = new \GuzzleHttp\Client();
        $credentials = base64_encode("e91cba03-507a-458a-9119-7663fcc7abe7:''");
        $call = $client->post('https://api.m-pay.com.au/mAccount/v1/transactions',["headers" => [
            "Authorization" => "Basic " . $credentials
        ],
            'json' => $param]);
       return response()->json([
           'status' => 200,
           'message' =>  json_decode((string)$call->getBody(), true)
       ]);
    }

}


