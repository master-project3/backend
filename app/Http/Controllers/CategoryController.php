<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class CategoryController extends Controller
{
    public function createCategory(Request  $request){
        try{
            $request->validate([
                'name' => "required|unique:category,name"
            ]);
        }
        catch (ValidationException $ex){
            return response()->json([
                'status' => 422,
                'message' => $ex->errors()
            ],422);
        }
        try{
            Category::create($request->all());
            return response()->json([
                'status' => 200,
                'message' => 'Skill/Category created Successfully'
            ]);
        }
        catch (\Exception $exception){
            return response()->json([
                'status' => 500,
                'message' => $exception->getMessage()
            ],500);
        }

    }

    public function updateCategory(Request  $request,$id){
        try{
            $request->validate([
                'name' => "required|unique:category,name,".$id
            ]);
        }
        catch (ValidationException $ex){
            return response()->json([
                'status' => 422,
                'message' => $ex->errors()
            ],422);
        }
        try{
           $vcategory = Category::find($id)->update($request->all());
            return response()->json([
                'status' => 200,
                'message' => 'Skill/Category updated Successfully'
            ]);
        }
        catch (\Exception $exception){
            return response()->json([
                'status' => 500,
                'message' => $exception->getMessage()
            ],500);
        }

    }

    public function deleteCategory($id){
        try{
            Category::findOrfail($id)->delete();
            return response()->json([
                'status' => 200,
                'message' => "Skill/Category deleted successfully"
            ]);
        }
        catch (\Exception $exception){
            return response()->json([
                'status' => 500,
                'message' => $exception->getMessage()
            ],500);
        }
    }
}
