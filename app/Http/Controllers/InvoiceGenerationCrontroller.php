<?php

namespace App\Http\Controllers;

use App\JobsClass\BaseEvent;
use App\Models\EmployeeTimesheet;
use App\Models\Invoice;
use App\Models\Jobs;
use App\Models\Payslips;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Recurr\Rule;
use Recurr\Transformer\ArrayTransformer;
use Recurr\Transformer\ArrayTransformerConfig;
use Recurr\Transformer\Constraint\AfterConstraint;

class InvoiceGenerationCrontroller extends Controller
{
    public $mappings = array(
        // Basic event properties. This is not all properties, only those needed explicitly
        // while processing recurrence. The others are read and set generically.
        'id' => 'id',
        'start_date' => 'start_date',
        'end_date' => 'end_date',
        'r_end_date' => 'end_date',
        // Recurrence-specific properties needed for processing recurring events:
        'rrule' => 'rrule',
        'duration' => 'duration',
        'orig_event_id' => 'origid',
        'recur_edit_mode' => 'redit',
        'recur_instance_start' => 'ristart',
        'recur_series_start' => 'rsstart',
        // Recurrence exceptions
        'exdate' => 'ex_date'
    );

    public function __construct()
    {

    }

    public function listAllInvoice(){
        $invoices = Invoice::where('status','paid')->get();
        foreach ($invoices as $key => $invoice){
            $invoices[$key]['client_name'] = $invoice->client->name;
            $invoices[$key]['job_name'] = $invoice->job->title;
        }
        return response()->json([
            'status' => 200,
            'payload' => $invoices
        ]);
    }

    public function generateInvoice(){
        self::generatePayslip();
        $jobs = $event = Jobs::get();
        $now =   Carbon::now()->startOfDay();
        foreach ($jobs as $job){
            if(!($next_occurance = self::processEachJobs($job,$now))) continue;
            $payment_frequency = $job->paid_frequency;
            $base_rate = $job->base_rate;
            switch ($payment_frequency){
                case "weekly":{
                    $amount = $base_rate/7;break;
                }
                case "fortnightly":
                {
                    $amount = $base_rate/14;break;
                }
                case "monthly":{
                    $amount = $base_rate/ (2*14);break;
                }
                default:{
                    break;
                }


            }
            $amount = round($amount,2);
            $data = [
                'client_id' => $job->client_id,
                'job_id' => $job->id,
                'status' => "pending",
                'description' => "Invoice",
                'amount' => $amount,
                'period_for_start_date' =>$now ,
                'period_for_end_date' => $next_occurance
            ];

            $create_invoice = Invoice::create($data);
            $client = $job->client->toArray();
            $client['job_name'] = $job->title;
            $client['amount'] = $amount;
            $client['invoice_id'] = $create_invoice->id;
            $client['created_at']  = $now;
            $client['due'] = $now->addDays(7);
            $client['payment_frequency'] = $payment_frequency;
            Mail::send('mail.test', $client, function($message) use ($client) {
                $message->to($client['email'], $client['name'])->subject
                ('Your '. $client['payment_frequency']. " Invoice");
                $message->from('no-reply-invoice@jobfitts.com','Jobfitts Consultant');
            });

        }
    }

    public static function processEachJobs($job,$current_date){
        $rule =  $job->rrule;
        $last_day =  $current_date->subDays(1);
        $rrule = new Rule(
            $rule,
            $last_day,
            null
        );
        $next_run_at = self::nextOccurrence($rule, $rrule, $last_day,1);

        if($next_run_at == null || $next_run_at == '') return false;

        $diff_in_days = $current_date->startOfDay()->diffInDays($next_run_at->startOfDay());

        if($diff_in_days != 1) return false;
        return $next_run_at;


    }

    public static function nextOccurrence($ruleText, Rule $rrule, Carbon $lastRun = null, $count = 0)
    {
        $maxCount = $rrule->getCount();

        if ($maxCount !== null && $count >= $maxCount)
        {
            return null;
        }

        $constraint = null;
        $arrayTransformer = new ArrayTransformer();
        $transformerConfig = new ArrayTransformerConfig();
        $transformerConfig->enableLastDayOfMonthFix();
        $transformerConfig->setVirtualLimit(2);
        $arrayTransformer->setConfig($transformerConfig);

        // if there is a last run supplied, meaning we are looking
        // for the next date, not the first date,
        // we will be adding an after contstraint
        if ($lastRun !== null)
        {
            // if there was a last run, and the
            // original ruleText just happens
            // to contain a DTSTART, we *MUST* set the rrule object
            // to use now's DTSTART instead... otherwise, the
            // virtual limit will be surpassed and our result will be null
            if (str_contains($ruleText, 'DTSTART=')) {
                $rrule->setStartDate($lastRun);
            }

            $constraint = new AfterConstraint($lastRun, false);
        }

        $occurrences = $arrayTransformer->transform($rrule, $constraint)->map(function($occurrence) {
            return Carbon::instance($occurrence->getStart());
        });

        if (!$occurrences->isEmpty())
        {
            return $occurrences->first();
        }
        else
        {
            return null;
        }
    }

    public static function generatePayslip(){
        DB::beginTransaction();
        $timesheets = EmployeeTimesheet::where('is_processed',0)->get();

        foreach ($timesheets as $timesheet){
            $timesheet->update([
                'is_processed' => 1
            ]);
           $employee = $timesheet->employee;
           $base_rate = $employee->base_rate;

            $hours = Carbon::parse($timesheet->end_date_time)->diffInHours(Carbon::parse($timesheet->start_date_time));
            if($timesheet->is_special_shift != 0){
                $amount = $timesheet->special_rate * $hours;
            }
            else{
                $amount = $base_rate * $hours;
            }

            $request = [
                'job_id' => $timesheet->job_id,
                'employee_id' => $timesheet->employee_id,
                'status' => 'pending',
                'description' => 'Payslip',
                'amount' => $amount,
                'period_for_start_date' => $timesheet->start_date_time,
                'period_for_end_date' => $timesheet->end_date_time
            ];
            Payslips::create($request);

        }
        DB::commit();
    }
}
