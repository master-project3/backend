<?php

namespace App\Http\Controllers;

use App\Models\Roles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class RoleController extends Controller
{
    //


    public function listAllRoles(Request $request){
        try{
           return response()->json([
               'status' => 200,
               'payload' => Roles::all()
           ]);
        }
        catch (\Exception $ex){
            return response()->json([
                'status' => 500,
                'message' => $ex->getMessage()
            ],500);
        }
    }

    public function storeRole(Request $request){
        try{
            DB::beginTransaction();
            $this->validate($request,[
               'name' => 'required|unique:roles,name',
                'display_name'=> 'required|string',
                'description' => 'required|string'
            ]);
        }
        catch (ValidationException   $exception){
            return response()->json([
                'status' => 422,
                'message' => $exception->errors()
            ],422);
        }
        try{

           Roles::create($request->all());
           DB::commit();
           return response()->json([
               'status' => 200,
               'message' => "Role created successfully"
           ]);
        }
        catch (\Exception $exception){
            return response()->json([
                'status' => 500,
                'message' => $exception->getMessage()
            ],500);
        }
    }

    public function updateRole(Request $request, $id){
        try{
            DB::beginTransaction();
            $this->validate($request,[
                'name' => 'required|unique:roles,name,'.$id,
                'display_name'=> 'required|string',
                'description' => 'required|string'
            ]);
        }
        catch (ValidationException   $exception){
            return response()->json([
                'status' => 422,
                'message' => $exception->errors()
            ],422);
        }
        try{
            $role = Roles::findOrFail($id);
            if(in_array($role->name,['admin','service-supervisor','staff-manager','payroll-officer']) && $role->name != $request->name){
                return response()->json([
                    'status' => 403,
                    'message' => "Roles admin,service-supervisor,staff-manager and payroll-officer could not e updated"
                ],403);
            }
            $role->update($request->all());
            DB::commit();
            return response()->json([
                'status' => 200,
                'message' => "Role updated successfully"
            ]);
        }
        catch (\Exception $exception){
            return response()->json([
                'status' => 500,
                'message' => $exception->getMessage()
            ],500);
        }
    }
}
