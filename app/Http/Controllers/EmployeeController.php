<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Employees;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class EmployeeController extends Controller
{

    public function createEmployee(Request $request){
        try {

            $this->validate($request,[
                'name' => "required|string|max:255",
                'email' => 'required|email|unique:App\Models\Employees,email',
                'skill' => "required|string",
                'description' => 'string',
                'category_id' => 'required|exists:category,id',
                'address' => 'required|string',
                'base_rate' => 'required|integer|min:1',
                'frequency' => 'required|in:hourly,daily,weekly,monthly',
                'photo' =>  'image|mimes:jpeg,png,jpg,svg|max:2048',
                'phone_number' =>  ['required','regex:/^\({0,1}((0|\+61)(2|4|3|7|8)){0,1}\){0,1}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{1}(\ |-){0,1}[0-9]{3}$/'],
                'license_number' => 'required|string',
                'status' => 'required|boolean',
                'bsb_number' => 'required|string',
                'account_number' => 'required|string'
            ]);
        }
        catch (ValidationException   $exception){
            return response()->json([
                'status' => 422,
                'message' => $exception->errors()
            ],422);
        }
        try{
            $photo = '';
            if($request->has('photo')){
                $imageName = time().'.'.$request->photo->getClientOriginalExtension();
                $url = "/images/".$imageName;

                $request->photo->move(public_path('/images'), $imageName);
                $photo = $url;

            }
            $request_array = $request->except('photo');
            $request_array['photo'] = $photo;


            $createEmployee = Employees::create($request_array);
            return response()->json([
                'status' => 200,
                'message' => 'Employee created successfully.'
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                'status' => 500,
                'message' => $ex->getMessage()
            ],500);
        }
    }

    public function updateEmployee($id,Request $request){
        try {

            $this->validate($request,[
                'name' => "required|string|max:255",
                'email' => 'required|email|unique:App\Models\Employees,email,'.$id,
                'skill' => "required|string",
                'category_id' => 'required|exists:category,id',
                'description' => 'string',
                'address' => 'required|string',
                'base_rate' => 'required|integer|min:1',
                'frequency' => 'required|in:hourly,daily,weekly,monthly',
                'photo' => 'string',
                'phone_number' =>  ['required','regex:/^\({0,1}((0|\+61)(2|4|3|7|8)){0,1}\){0,1}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{1}(\ |-){0,1}[0-9]{3}$/'],
                'license_number' => 'required|string',
                'status' => 'required|boolean',
                'bsb_number' => 'required|string',
                'account_number' => 'required|string'
            ]);
        }
        catch (ValidationException   $exception){
            return response()->json([
                'status' => 422,
                'message' => $exception->errors()
            ],422);
        }
        try{
            $getEmployee = Employees::findOrFail($id);

            $createEmployee = $getEmployee->update($request->all());
            return response()->json([
                'status' => 200,
                'message' => 'Employee updated successfully.'
            ]);
        }
        catch (ModelNotFoundException $ex){
            return response()->json([
                'status' => 404,
                'message' => 'Employee not found.'
            ],404);
        }
        catch (\Exception $ex){
            return response()->json([
                'status' => 500,
                'message' => $ex->getMessage()
            ],500);
        }
    }

    public function getEmployeeDetails($id){
        try {
            $getEmployee = Employees::findOrFail($id);
            return response()->json([
                'status' => 200,
                'payload' => $getEmployee
            ]);
        }
        catch (ModelNotFoundException $exception){
            return response()->json([
                'status' => 404,
                'message' => "Employee could not found"
            ]);
        }
    }

    public function listAllEmployee(Request $request){
        try{
            if($request->category_id){
                return response()->json([
                    'status' => 200,
                    'payload' => Employees::where('category_id',$request->category_id)->orderBy('id','desc')->get()
                ]);
            }
            return response()->json([
                'status' => 200,
                'payload' => Employees::orderBy('id','desc')->get()
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                'status' => 500,
                'message' => $ex->getMessage()
            ],500);
        }
    }

    public function getCategoryList(){
        return response()->json([
            'status' => 200,
            'payload' => Category::all()
        ]);
    }

}
