<?php

namespace App\Http\Controllers;

use App\Models\Clients;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Schema;
use Illuminate\Validation\ValidationException;

class ClientsController extends Controller
{
    //

    public function createClient(Request $request){
        try {
            $message = [
                "abn_validation" => "The entered :attribute is not a valid format."
            ];

            $this->validate($request,[
                'name' => "required|string",
                'email' => 'required|email|unique:App\Models\Clients,email',
                'abn' => "required|abn_validation", //https://abr.business.gov.au/Help/AbnFormat
                'address' => "required|string",
                'url' =>'required|url',
                'contact_no' => ['required','regex:/^\({0,1}((0|\+61)(2|4|3|7|8)){0,1}\){0,1}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{1}(\ |-){0,1}[0-9]{3}$/'],
                'status' => 'required|in:0,1'
            ],$message);
        }
        catch (ValidationException   $exception){
            return response()->json([
                'status' => 422,
                'message' => $exception->errors()
            ],422);
        }
        try {
            DB::beginTransaction();
            $createClient = Clients::create($request->all());
            DB::commit();
            return response()->json([
                'status' => 200,
                'message' => "Client created successfully"
            ]);
        }
        catch (\Exception $exception){
            return response()->json([
                'status' => '500',
                'message' => $exception->getMessage()
            ],500);
        }
    }

    public function updateClient($id,Request $request){
        try {
            $message = [
                "abn_validation" => "The entered :attribute is not a valid format."
            ];
            $this->validate($request,[
                'name' => "required|string",
                'email' => 'required|email|unique:App\Models\Clients,email,'.$id,
                'abn' => "required|abn_validation", //https://abr.business.gov.au/Help/AbnFormat
                'address' => "required|string",
                'url' =>'required|url',
                'contact_no' => ['required','regex:/^\({0,1}((0|\+61)(2|4|3|7|8)){0,1}\){0,1}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{1}(\ |-){0,1}[0-9]{3}$/'],
                'status' => 'required|in:0,1'
            ],$message);
        }
        catch (ValidationException   $exception){
            return response()->json([
                'status' => 422,
                'message' => $exception->errors()
            ],422);
        }
        try {
            $getClient = Clients::findOrFail($id);
            $updateClient = $getClient->update($request->all());
            DB::commit();
            return response()->json([
                'status' => 200,
                'message' => "Client updated successfully"
            ]);
        }
        catch (\Exception $exception){
            return response()->json([
                'status' => '500',
                'message' => $exception->getMessage()
            ],500);
        }
    }

    public function getClient($id){
        try {
            $getCLient = Clients::findOrfail($id);
            return response()->json([
                'status' => 200,
                'payload' => $getCLient
            ]);
        }
        catch (ModelNotFoundException $exception){
            return response()->json([
                'status' => 404,
                'message' => "Client could not be found"
            ]);
        }
        catch (\Exception $exception){
            return response()->json([
                'status' => 500,
                'message' => $exception->getMessage()
            ],500);
        }
    }

    public function listAllClients(Request  $request){
        try{
            $this->validate($request,[
                'search_value' => 'string'
            ]);
        }
        catch (ValidationException $exception){
            return response()->json([
                'status'=> 422,
                'message' => $exception->errors()
            ],422);
        }
        try{
            $search_value = '';
            if($request->has('search_value')){
                $search_value = $request->search_value;
            }
             $clients =  $this->searchCLients($search_value);
            return response()->json([
                'status' => '200',
                'payload' => $clients
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                'status' => 500,
                'message' => $ex->getMessage()
            ],500);
        }


    }

    private function searchCLients($search = ''){
        $columnsList = Schema::getColumnListing("clients");
        $search = "%".$search."%";
        $clients = Clients::where(function ($query) use ($columnsList, $search) {
            foreach ($columnsList as $key => $column) {
                if ($key === 0) {
                    $query->where("clients.".$column, "like", $search);

                } else {


                    $query->orWhere($column, "like", $search);
                }
            }


        });
        return $clients->orderBy('id', "desc")->get();
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteCLient($id){
        try {
            $getCLient = Clients::findOrfail($id)->delete();
            return response()->json([
                'status' => 200,
                'payload' => "client deleted successfully"
            ]);
        }
        catch (ModelNotFoundException $exception){
            return response()->json([
                'status' => 404,
                'message' => "Client could not be found"
            ]);
        }
        catch (\Exception $exception){
            return response()->json([
                'status' => 500,
                'message' => $exception->getMessage()
            ],500);
        }
    }


    public function sendEmail($id){
        $client = Clients::findOrFail($id)->toArray();
        Mail::send('mail.test', $client, function($message) use ($client) {
            $message->to($client['email'], $client['name'])->subject
            ('Invoice');
            $message->from('no-reply-invoice@jobfitts.com','Jobfitts Consultant');
        });
        echo "Basic Email Sent. Check your inbox.";
    }

}
