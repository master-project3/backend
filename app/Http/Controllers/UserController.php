<?php

namespace App\Http\Controllers;

use App\Models\Roles;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\ValidationException;

class UserController extends Controller
{
    public function __construct()
    {

    }

    public function resetPassword(Request $request){
        try{
            $this->validate($request,[
                'email' => 'required|email|exists:users,email',
            ]);
        }
        catch (ValidationException   $exception){
            return response()->json([
                'status' => 422,
                'message' => $exception->errors()
            ],422);
        }
        try{
            DB::beginTransaction();
            $user_get = User::where('email',$request->email)->get()->toArray();
            $password = \Str::random(6)."#";
            $user =  User::where('email',$request->email)->update([
                "password" => app('hash')->make($password)
            ]);

            $user_get[0]['password'] = $password;
            $user_profile = $user_get[0];


            Mail::send('mail.password_reset', $user_profile, function($message) use ($user_profile) {
                $message->to($user_profile['email'], $user_profile['name'])->subject
                ('Password Reset');
                $message->from('no-reply@jobfitts.com','Jobfitts Consultant');
            });

            DB::commit();
            return response()->json([
                'status' => '200',
                "message" => 'Check your inbox for your password information'
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                'status' => 500,
                'message' => $ex->getMessage()
            ],500);
        }
    }

    public function getSpecificUserInfo($id){
        try {
            $user = User::findOrFail($id);
            $roles = $user->roles;
            $user['role_id'] = $roles[0]->id;
            return response()->json([
                'status' => 200,
                'payload' => $user
            ]);
        }
        catch (ModelNotFoundException $ex){
            return response()->json([
                'status' => 404,
                'message' => "User not found"
            ],404);
        }
        catch (\Exception $ex){
            return response()->json([
                'status' => 500,
                'message' => $ex->getMessage()
            ],500);
        }
    }

    public function getLoggedInUserDetails(){
        return response()->json([
            'status' => 200,
            'payload' => Auth::user()
        ]);
    }

    public function updateLoggedInuserProfile(Request $request){
        try{
            $this->validate($request,[
                'name' => 'required|string',
                'address' => 'required|string'
            ]);
        }
        catch (ValidationException   $exception){
            return response()->json([
                'status' => 422,
                'message' => $exception->errors()
            ],422);
        }
        try{
            $user = Auth::user()->update($request->all());
            return response()->json([
                'status' => 200,
                'message' => 'Your profile has been updated successfully.'
            ]);
        }
        catch (\Exception $exception){
            return response()->json([
                'status' => 500,
                'message' => $exception->getMessage()
            ],500);
        }
    }

    public function listAllUser(){
        try{
            $user = User::with('roles')->get();
            return response()->json([
                'status' => 200,
                'payload' => $user
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                'status' => 500,
                'message' => $ex->getMessage()
            ],500);
        }
    }

    public function createUser(Request  $request){
        try{
            $this->validate($request,[
                'name' => 'required|string',
                'address' => 'required|string',
                'email' => 'required|email|unique:users,email',
                'password' => 'required|string',
                'role_id' => 'required|exists:roles,id'
            ]);
        }
        catch (ValidationException   $exception){
            return response()->json([
                'status' => 422,
                'message' => $exception->errors()
            ],422);
        }
        try{
            DB::beginTransaction();
            $create_user = User::create($request->all());
            $get_user = User::find($create_user->id);
            $role = Roles::find($request->role_id);
            $user = $get_user->toArray();
            $user['role_name'] =$role->display_name;
            $user['password'] = $request->password;
            $user['type'] ='created';
            $get_user->roles()->attach([$request->role_id]);
            Mail::send('mail.user_created', $user, function($message) use ($user) {
                $message->to($user['email'], $user['name'])->subject
                ('User Created');
                $message->from('no-reply@jobfitts.com','Jobfitts Consultant');
            });
            DB::commit();
            return response()->json([
                'status' => 200,
                'message' => 'User created successfully.'
            ]);
        }
        catch (\Exception $exception){
            return response()->json([
                'status' => 500,
                'message' => $exception->getMessage()
            ],500);
        }
    }

    public function updateUserDetail(Request $request, $id){
        try{
            $this->validate($request,[
                'name' => 'required|string',
                'address' => 'required|string',
                'email' => 'required|email|unique:users,email,'.$id,
                'password' => 'required|string',
                'role_id' => 'required|exists:roles,id'
            ]);
        }
        catch (ValidationException   $exception){
            return response()->json([
                'status' => 422,
                'message' => $exception->errors()
            ],422);
        }
        try{
            if(Auth::id() == $id){
                return response()->json([
                    'message' => "You cannot update your profile from here.",
                    'status' => 403
                ],403);
            }
            DB::beginTransaction();

            $get_user = User::find($id);
            $get_user->update($request->all());//update user
            $role = Roles::find($request->role_id);
            $user = $get_user->toArray();
            $user['role_name'] =$role->display_name;
            $user['password'] = $request->password;
            $user['type'] ='updated';
            $get_user->roles()->sync([$request->role_id]);
            Mail::send('mail.user_created', $user, function($message) use ($user) {
                $message->to($user['email'], $user['name'])->subject
                ('User Updated');
                $message->from('no-reply@jobfitts.com','Jobfitts Consultant');
            });
            DB::commit();
            return response()->json([
                'status' => 200,
                'message' => 'User updated successfully'
            ]);
        }
        catch (\Exception $exception){
            return response()->json([
                'status' => 500,
                'message' => $exception->getMessage()
            ],500);
        }
    }

    public function deleteUser($id){
        if(Auth::id() == $id){
            return response()->json([
                'status' => 403,
                'message' => 'You cannot delete yourself'
            ],403);
        }
        try{
            User::findOrfail($id)->delete();
            return response()->json([
                'status' => 200,
                'message' => 'User deleted successfully'
            ],200);
        }
        catch (ModelNotFoundException $exception){
            return response()->json([
                'status' => 404,
                'message' =>$exception->getMessage()
            ],404);
        }
        catch (\Exception $ex){
            return response()->json([
                'status' => 500,
                'message' => $ex->getMessage()
            ],500);
        }
    }
}
