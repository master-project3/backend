<?php

namespace App\Http\Controllers;

use App\Models\Employees_Avaibility;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EmployeeAvailabilityController extends Controller
{


    public function createAvaibility(Request  $request){
        try{
            $this->validate($request,[
                'sunday' => "boolean",
                "monday" =>"boolean",
                "tuesday" => "boolean",
                "wednesday" => "boolean",
                "thursday" =>"boolean",
                "friday" => "boolean",
                "saturday" => "boolean",
                'status' => "boolean",
                "employee_id" => "required|exists:employees,id",
                "availability_for_start_date" => "required|date_format:Y-m-d",
                "availability_for_end_date" => "required|date_format:Y-m-d|after:availability_for_start_date"


            ]);
        }

        catch (\Illuminate\Validation\ValidationException $exception){
            return response()->json([
                'status' => 422,
                'message' => $exception->errors()
            ],422);
        }
        try{
        DB::beginTransaction();
        $change_status_of_past_records = Employees_Avaibility::where('employee_id',$request->employee_id)
            ->where('status',1)->update(['status' => 0]);

        Employees_Avaibility::create($request->all());
        DB::commit();
        return response()->json([
            'status' => 200,
            "message" => "Employee Availability created"
        ]);


       }
        catch (\Exception $exception){
            return response()->json([
                'status' => 500,
                "message" => $exception->getMessage()
            ],500);
        }
    }

    public function updateAvaibility($id,Request  $request){
        try{
            $this->validate($request,[
                'sunday' => "boolean",
                "monday" =>"boolean",
                "tuesday" => "boolean",
                "wednesday" => "boolean",
                "thursday" =>"boolean",
                "friday" => "boolean",
                "saturday" => "boolean",
                'status' => "required|boolean",
                "employee_id" => "required|exists:employees,id",
                "availability_for_start_date" => "required|date_format:Y-m-d",
                "availability_for_end_date" => "required|date_format:Y-m-d|after:availability_for_start_date"


            ]);
        }

        catch (\Illuminate\Validation\ValidationException $exception){
            return response()->json([
                'status' => 422,
                'message' => $exception->errors()
            ],422);
        }
        try{
            DB::beginTransaction();
            $change_status_of_past_records = Employees_Avaibility::where('employee_id',$request->employee_id)
                ->where('status',1)->update(['status' => 0]);

            $get_employee = Employees_Avaibility::findOrFail($id)->update($request->all());


            DB::commit();
            return response()->json([
                'status' => 200,
                "message" => "Employee Availability Updated Successfully"
            ]);


        }
        catch (\Exception $exception){
            return response()->json([
                'status' => 500,
                "message" => $exception->getMessage()
            ],500);
        }
    }

    public function listAllEmployeeAvaibility(Request  $request){
        try{
            $this->validate($request,[
                'employee_id' => "required",
                "availability_for_end_date" => "date_format:Y-m-d|after:availability_for_start_date",
                "availability_for_start_date" => "required_with:availability_for_end_date|date_format:Y-m-d",
            ]);
        }
        catch (\Illuminate\Validation\ValidationException $exception){
            return response()->json([
                'status' => 422,
                'message' => $exception->errors()
            ],422);
        }
        try{
        if(!$request->has('availability_for_start_date')){

            $employee = Employees_Avaibility::where('employee_id',$request->employee_id)->
            orderBy("availability_for_start_date",'desc')->
            orderBy('availability_for_end_date','desc')->
            get();
        }

        if($request->has('availability_for_start_date')){
            $employee = Employees_Avaibility::where('employee_id',$request->employee_id)->
            where("availability_for_start_date",">=",$request->availability_for_start_date)->
            orderBy("availability_for_start_date",'desc')->
            orderBy('availability_for_end_date','desc')->
            get();
        }
        if($request->has('availability_for_start_date') && $request->has('availability_for_end_date')){
            $employee = Employees_Avaibility::where('employee_id',$request->employee_id)->
            where("availability_for_start_date",'>=',Carbon::parse($request->availability_for_start_date))->
            where("availability_for_end_date","<=",Carbon::parse($request->availability_for_end_date))->
            orderBy("availability_for_start_date",'desc')->
            orderBy('availability_for_end_date','desc')->
            get();
        }
        if(!$request->has('availability_for_start_date')){
            $employee = Employees_Avaibility::where('employee_id',$request->employee_id)->
            orderBy("availability_for_start_date",'desc')->
            orderBy('availability_for_end_date','desc')->
            get();
        }


        return response()->json([
            'status' => 200,
            'payload' => $employee
        ]);
        }
        catch (\Exception $exception){
            return response()->json([
                'status' => 500,
                "message" => $exception->getMessage()
            ],500);
        }
    }
}
