<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{



    public function login(Request $request){
        try{
            $this->validate($request,[
                'email' => 'required|max:255',
                'password' => 'required|max:255'
            ]);
        }
        catch (ValidationException   $exception){
            return response()->json([
                'status' => 422,
                'message' => $exception->errors()
            ],422);
        }
        try{
            $clientInfo = DB::table("oauth_clients")->where("id", 2)->first();
            if (!$clientInfo) {
                return response()->json([
                    "status" => "404",
                    "message" => "Client could not be found."
                ], 404);
            }

            $client_id = $clientInfo->id;
            $client_secret = $clientInfo->secret;
            $proxy = Request::create(
                '/oauth/token',
                'post',
                [
                    'grant_type' => "password",
                    'client_id' => $client_id,
                    'client_secret' => $client_secret,
                    'username' => $request["email"],
                    'password' => $request["password"]
                ]
            );
            $data = app()->handle($proxy);
            if($data->getStatusCode() !== 200){
                return response()->json([
                    'status' => 401,
                    'message' => "Email or password does not match"
                ],401);
            }
            $getTokenInfo = json_decode($data->getContent(),true);
            /*
             * checking user exist by searching through email
             * */

            $user = User::where([["email", $request["email"]]])->first();
            $role = $user->roles;

            $getTokenInfo['user'] = $user;
            return response()->json([
                'status' => 200,
                'payload' => $getTokenInfo
            ]);



        }
        catch (\Exception $exception){

        }

    }
}
