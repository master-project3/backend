<?php

namespace App\Http\Controllers;

use App\Models\Employees;
use App\Models\Jobs;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class JobAssignmentController extends Controller
{

    public function getAssignedJob(Request $request){
        try{
            $this->validate($request,[
                'job_id' => "exists:jobs,id",
                'employee_id' => 'exists:employees,id'
            ]);
        }
        catch (ValidationException $ex){
            return response()->json([
                'status' => 200,
                'message' => $ex->errors()
            ],422);
        }
        try{
            if($request->has('job_id') && $request->has('employee_id')){
                $get_jobs = Jobs::findOrFail($request->job_id)->employee()->
                where('employee_id',$request->employee_id)->
                    get();
                return response()->json([
                    'status' => 200,
                    'payload' => $get_jobs
                ]);
            }
            if($request->has('job_id') && !$request->has('employee_id') ){
                $get_jobs = Jobs::findOrFail($request->job_id)->employee()->
                get();
                return response()->json([
                    'status' => 200,
                    'payload' => $get_jobs
                ]);
            }
            if($request->has('employee_id') && !$request->has('jobs_id') ){
                $get_employee = Employees::findOrFail($request->employee_id)->job()->
                get();
                return response()->json([
                    'status' => 200,
                    'payload' => $get_employee
                ]);
            }
            return response()->json([
                'status' => 200,
                'payload' => []
            ]);


        }
        catch (\Exception $exception){
            return response()->json([
                'status' => 500,
                'message' => $exception->getMessage()
            ],500);
        }
    }

    public function assignJobs(Request $request){
        try{
            $this->validate($request,[
                'job_id' => "required|exists:jobs,id",
                'employee_id' => 'required|exists:employees,id'
            ]);
        }
        catch (ValidationException $ex){
            return response()->json([
                'status' => 200,
                'message' => $ex->errors()
            ],422);
        }
        try{
            $job = Jobs::findOrFail($request->job_id);
            $get_jobs = Jobs::findOrFail($request->job_id)->employee()->
            get();

            $get_employee = Employees::WhereIn('id',$request->employee_id);
            foreach ($get_employee as $employee){
                if($employee->category_id != $job->category_id) {
                    return response()->json([
                        'status' => 403,
                        'message' => 'One of the employee does not belongs to the category'
                    ],403);
                }
            }
            $total_assigned_jobs = count($get_jobs);
            $requested_employee  = count($request->employee_id);
            $total_employee = $total_assigned_jobs + $requested_employee;
            if($job->no_of_employee < $total_employee){
                return response()->json([
                    'status' => 403,
                    'message' => "Number of employee exceeds the required job employee limits."
                ],403);
            }
            $job->employee()->attach($request->employee_id);
            return response()->json([
                'status' => 200,
                'message' => 'Employee is successfully assigned to the job'
            ]);
        }
        catch (QueryException $exception){
            $message = $exception->getMessage();
            if($exception->getCode() == 23000){
                $message = "Duplicate Entry";
            }
            return response()->json([
                'status' => 500,
                "message" => $message
            ],500);
        }
        catch (\Exception $ex){
            return response()->json([
                'status' => 500,
                'message' => $ex->getMessage()
            ],500);
        }
    }

    public function removeJobs(Request  $request){
        try{
            $this->validate($request,[
                'job_id' => "required|exists:jobs,id",
                'employee_id' => 'required|exists:employees,id'
            ]);
        }
        catch (ValidationException $ex){
            return response()->json([
                'status' => 200,
                'message' => $ex->errors()
            ],422);
        }
        try{
            $job = Jobs::findOrFail($request->job_id);
            $job->employee()->detach($request->employee_id);
            return response()->json([
                'status' => 200,
                'message' => 'Employee is successfully removed from the job'
            ]);
        }
        catch (QueryException $exception){
            $message = $exception->getMessage();
            if($exception->getCode() == 23000){
                $message = "Duplicate Entry";
            }
            return response()->json([
                'status' => 500,
                "message" => $message
            ],500);
        }
        catch (\Exception $ex){
            return response()->json([
                'status' => 500,
                'message' => $ex->getMessage()
            ],500);
        }
    }
}
