<?php

namespace App\Http\Controllers;

use App\Models\Invoice;
use App\Models\Payslips;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class PayrollController extends Controller
{
    public function payNow(Request  $request){
        try{
            $this->validate($request,[
                'invoice_id' => 'required|exists:invoices,id',
                'amount' =>"required|numeric|between:1,99999"
            ]);
        }
        catch (ValidationException   $exception){
            return response()->json([
                'status' => 422,
                'message' => $exception->errors()
            ],422);
        }
        try{
            /**
             * Direct debit from client to company
             */
            $jayParsedAry = [
                "totalAmount" => $request->amount,
                "paymentSource" => "directDebit",
                "lodgementReference" => "Ref-$request->invoice". rand(0,100).rand(0,100),
                "directDebit" => [
                    "bsbNumber" => "032-000",
                    "accountNumber" => "56654983",
                    "accountName" => "AShish 1234"
                ]
            ];
            $invoice = Invoice::where('id',$request->invoice_id)->where('amount',$request->amount)->where('status','pending')->get();
            if($invoice->count() == 0){
                return response()->json([
                    'status' => 403,
                    'message' => 'Already made the payment or invalid request'
                ]);
            }


//        $jayParsedAry = [
//            "uniqueReference" => "Ref-$request->invoice". rand(0,100).rand(0,100),
//            "totalAmount" => $request->amount,
//            "printUniqueReferenceOnStatement" => true,
//            "description" => "mAccount to DirectCredit",
//            "paymentSource" => "mAccount",
//            "mAccount" => [
//                "token" => "e91cba03-507a-458a-9119-7663fcc7abe7"
//            ],
//            "disbursements" => [
//                [
//                    "disbursementMethod" => "directCredit",
//                    "description" => "mAccount to DirectCredit",
//                    "toDirectCreditDetails" => [
//                        "bsbNumber" => "802-985",
//                        "remitterName" => "AShish Kafle",
//                        "accountNumber" => "612344305",
//                        "accountName" => "Moneytech Payments"
//                    ],
//                    "amount" => $request->amount
//                ]
//            ]
//        ];
            $client = new Client();
            $credentials = base64_encode("e91cba03-507a-458a-9119-7663fcc7abe7:''");
            $result = $client->post('https://api.m-pay.com.au/financial/v2/transaction/execute',[
                "headers" =>[
                    "Authorization" => "Basic ". $credentials
                ],
                'json' => $jayParsedAry
            ]);



            $invoice = $invoice[0]->update([
                'status' => "paid"
            ]);
            return response()->json([
                'status' => '200',
                "message" => "Payment has been made. Thank you"
            ]);
        }
        catch (\Exception $exception){

        }
    }

    public function listPayroll(Request $request){
        try{
            $this->validate($request,[
                'employee_id' => 'required|exists:invoices,id',
                'start_date' =>"required|date_format:Y-m-d",
                'end_date' => "required|date_format:Y-m-d|after:start_date"
            ]);
        }
        catch (ValidationException   $exception){
            return response()->json([
                'status' => 422,
                'message' => $exception->errors()
            ],422);
        }

        $payrolls = Payslips::whereRaw(
            "(period_for_start_date >= ? AND period_for_end_date <= ?)",
            [$request->start_date, $request->end_date]
        )->where('employee_id',$request->employee_id)->where('status',"pending")->get();
        foreach ($payrolls as $key => $payroll){
            $employee = $payroll->employee()->first();

//               $payrolls[$key]['employee_name'] = $employee->name;
            $payrolls[$key]['employee_bsb_number'] = $employee->bsb_number;
            $payrolls[$key]['employee_account_number'] = $employee->account_number;
            $payrolls[$key]['due_days'] = 7;
            if($key == 0) {
                $payrolls[$key]['due_days'] = 4;
            }

        }
        return response()->json([
            'status' => 200,
            'payload' => $payrolls
        ]);
        try{  }
        catch (\Exception $exception){

        }
    }

    public function payToEmployee(Request  $request)
    {
        try {
            $this->validate($request, [
                'payslip_id' => 'required|exists:payslips,id',
            ]);
        } catch (ValidationException   $exception) {
            return response()->json([
                'status' => 422,
                'message' => $exception->errors()
            ], 422);
        }

        try {
            DB::beginTransaction();
            $payslip = Payslips::where('id', $request->payslip_id)->where('status', "pending")->first();

            if (!$payslip) {
                return response()->json([
                    'status' => 403,
                    'message' => 'Payslip does not exists or payment has already been made'
                ], 403);
            }
            $employee = $payslip->employee()->first();
            $jayParsedAry = [
                "uniqueReference" => "Ref-payslip-DB-$request->payslip_id" . rand(0, 10) . rand(0, 100),
                "totalAmount" => $payslip->amount,
                "printUniqueReferenceOnStatement" => true,
                "description" => "mAccount to DirectCredit",
                "paymentSource" => "mAccount",
                "mAccount" => [
                    "token" => "e91cba03-507a-458a-9119-7663fcc7abe7"
                ],
                "disbursements" => [
                    [
                        "disbursementMethod" => "directCredit",
                        "description" => "mAccount to DirectCredit",
                        "toDirectCreditDetails" => [
                            "bsbNumber" => "$employee->bsb_number",
                            "remitterName" => "$employee->name",
                            "accountNumber" => "$employee->account_number",
                            "accountName" => "$employee->name"
                        ],
                        "amount" => $payslip->amount
                    ]
                ]
            ];
            $client = new Client();
            $credentials = base64_encode("e91cba03-507a-458a-9119-7663fcc7abe7:''");
            $result = $client->post('https://api.m-pay.com.au/financial/v2/transaction/execute', [
                "headers" => [
                    "Authorization" => "Basic " . $credentials
                ],
                'json' => $jayParsedAry
            ]);
            $response = json_decode((string)$result->getBody(), true);
            if ($response['status'] == 'Ok') {
                $payslip->update([
                    'status' => "paid"
                ]);
            }
            DB::commit();
            return response()->json([
                'status' => 200,
                'message' => "Payment has successfully been made"
            ]);

        } catch (\Exception $exception) {
            return response()->json([
                'status' => 500,
                'message' => $exception->getMessage()
            ], 500);
        }
    }
        public function listAllPayroll(Request $request){
            try{
                $this->validate($request,[
                    'employee_id' => 'required|exists:employees,id',
                ]);
            }
            catch (ValidationException   $exception){
                return response()->json([
                    'status' => 422,
                    'message' => $exception->errors()
                ],422);
            }

            return response()->json([
                'status' => 200,
                'payload' => Payslips::where("employee_id",$request->employee_id)->where('status','paid')->get()
            ]);


    }


}
