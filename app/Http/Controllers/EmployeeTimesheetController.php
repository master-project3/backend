<?php

namespace App\Http\Controllers;

use App\Models\Jobs;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class EmployeeTimesheetController extends Controller
{


    public function getSpecificDateTimesheet($id){
        try{
            return response()->json([
                'status' => 200,
                'payload' => \App\Models\EmployeeTimesheet::findOrFail($id)
            ]);
        }
        catch (ModelNotFoundException $exception){
            return response()->json([
                'status' => 404,
                'message' => 'Timesheet Not found'
            ],404);
        }
        catch (\Exception $ex){

        }
    }


    public function getSpecialEmployeeTimesheet(Request $request){
        try{
            $this->validate($request,[
                'employee_id' => 'exists:employees,id',
                "job_id" => "exists:jobs,id",
                "start_date" => "date_format:Y-m-d",
                "end_date" => "required_with:start_date|date_format:Y-m-d|after:start_date"
            ]);
        }
        catch (ValidationException   $exception){
            return response()->json([
                'status' => 422,
                'message' => $exception->errors()
            ],422);
        }
        try{
            if($request->has('employee_id') && $request->has('job_id') && $request->has('start_date') ){
//                $get_timesheet = \App\Models\EmployeeTimesheet::where("employee_id",$request->employee_id)
//                ->where('job_id',$request->job_id)
//                    ->where
            }
            elseif($request->has('employee_id') && $request->has('job_id')){
                $get_timesheets = \App\Models\EmployeeTimesheet::where("employee_id",$request->employee_id)
                    ->where('job_id',$request->job_id)->get();
            }
            elseif($request->has('employee_id') ){
                $get_timesheets = \App\Models\EmployeeTimesheet::where("employee_id",$request->employee_id)->get();
            }
            elseif($request->has('job_id') ){
                $get_timesheets = \App\Models\EmployeeTimesheet::where("job_id",$request->job_id)->get();
            }
            else{
                $get_timesheets = [];
            }
            foreach ($get_timesheets as $key => $timesheet){
                $client = $timesheet->job()->get();
                $get_timesheets[$key]['job_title'] = $client[0]->title;
            }
            return response()->json([
                'status' => 200,
                "payload"=> $get_timesheets
            ]);
        }
        catch (\Exception $exception){
            return response()->json([
                'status'=> 500,
                'message' => $exception->getMessage()
            ],500);
        }
    }

    public function createTimesheet(Request  $request){
        try{
            $this->validate($request,[
                "employee_id" => "required|exists:App\Models\Employees,id",
                "job_id" => "required|exists:App\Models\Jobs,id",
                'start_date_time' => "required|date_format:Y-m-d H:i:s",
                "end_date_time" => "required|date_format:Y-m-d H:i:s|after:start_date_time",
                "is_special_shift" => "required|boolean",
                "special_rate" => "required_if:is_special_shift,1|integer"
            ]);
        }
        catch (ValidationException   $exception){
            return response()->json([
                'status' => 422,
                'message' => $exception->errors()
            ],422);
        }
        try{
            $jobs = Jobs::findOrFail($request->job_id);
            $employee = $jobs->employee()->get();
            if($employee->count() <= 0){
                return response()->json([
                    'status' => 403,
                    'message' => 'Employee has not been assigned to provided job.'
                ],403);
            }
            $create_timesheet = \App\Models\EmployeeTimesheet::create($request->all());
            return response()->json([
                'status' => 200,
                "message" => "Timesheet Created Successfully."
            ],200);
        }
        catch (\Exception $exception){
            return response()->json([
                'status'=> 500,
                'message' => $exception->getMessage()
            ],500);
        }
    }

    public function updateTimesheet($id,Request  $request){
        try{
            $this->validate($request,[
                "employee_id" => "required|exists:employees,id",
                "job_id" => "required|exists:jobs,id",
                'start_date_time' => "required|date_format:Y-m-d H:i:s",
                "end_date_time" => "required|date_format:Y-m-d H:i:s|after:start_date_time",
                "is_special_shift" => "required|boolean",
                "special_rate" =>  "required_if:is_special_shift,1|nullable"
            ]);
        }
        catch (ValidationException $ex){
            return response()->json([
                'status' => 422,
                'message' => $ex->errors()
            ],422);
        }
        try{
            $jobs = Jobs::findOrFail($request->job_id);
            $employee = $jobs->employee()->get();
            if($employee->count() <= 0){
                return response()->json([
                    'status' => 403,
                    'message' => 'Employee has not been assigned to provided job.'
                ],403);
            }
            $getTimesheet = \App\Models\EmployeeTimesheet::findOrFail($id);
            $update_timesheet = $getTimesheet->update($request->all());
            return response()->json([
                'status' => 200,
                "message" => "Timesheet Updated Successfully."
            ],200);
        }
        catch (ModelNotFoundException $exception){
            return response()->json([
                'status' => 404,
                'message' => 'Timesheet Not found'
            ],404);
        }
        catch (\Exception $exception){
            return response()->json([
                'status'=> 500,
                'message' => $exception->getMessage()
            ],500);
        }
    }
}
