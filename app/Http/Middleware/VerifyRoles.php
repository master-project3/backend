<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VerifyRoles
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next,$allowed_roles)
    {
        if (!is_array($allowed_roles)) {
            $allowed_roles = explode('|', $allowed_roles);
        }
        /**
         * Fetch the existing role of user
         */
        $existing_roles = Auth::user()->roles;

        /**
         * Check any of the passed roles matches with the logged in user roles
         */
        foreach ($existing_roles as $existing_role){
            return $next($request);
//            foreach ($allowed_roles as $allowed_role){
//                if($allowed_role == $existing_role->name){
//                    return $next($request);
//                }
//            }
        }
        return response()->json([
            'status' => 403,
            'message' => "Permission Denied! make sure you have correct rights to access"
        ],403);

    }
}
