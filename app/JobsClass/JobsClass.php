<?php


namespace App\JobsClass;


use App\JobsClass\BaseEvent;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class JobsClass implements JobsInterface
{

    private $jobs;
    private $reminder;
    private $base_event;
    private $max_event_instances = 8000;//day limit
    private $date_format = 'Y-m-d H:i:s';
    private $exception_format = 'Y-m-d H:i:s';
    private $rrule_date_format = 'Ymd\\THis\\Z';
    private $event_exception;
    private $user;
    private $calendar_users;
    private $grant_type;
    public $mappings = array(
        // Basic event properties. This is not all properties, only those needed explicitly
        // while processing recurrence. The others are read and set generically.
        'id' => 'id',
        'start_date' => 'start_date',
        'end_date' => 'end_date',
        'r_end_date' => 'recurring_end_date',
        // Recurrence-specific properties needed for processing recurring events:
        'rrule' => 'rrule',
        'duration' => 'duration',
        'orig_event_id' => 'origid',
        'recur_edit_mode' => 'redit',
        'recur_instance_start' => 'ristart',
        'recur_series_start' => 'rsstart',
        // Recurrence exceptions
        'exdate' => 'ex_date'
    );

    public function __construct(\App\Repo\JobsInterface $job)
    {
        $this->jobs = $job;
    }

    public function getSpecificJobs($jobs_id)
    {
        try {

            $jobs = $this->jobs->getSpecificJobs($jobs_id, true);

            return $jobs;
        } catch (ModelNotFoundException $ex) {
            throw new ModelNotFoundException('Jobs could not be found.');
        }
    }

    public function getSpecificClientJobs($client_id, $start_date, $end_date)
    {
        $jobs = $this->jobs->getAllJobs($client_id, $start_date, $end_date);
        $matches = array();
        $final_jobs= $jobs->toArray();
        $matches = array_merge($matches,$this->generateInstances($jobs,$start_date,$end_date));
        return $matches;

    }

    public function listAllJobs(){
        return $this->jobs->listAllJobsWithoutClient();
    }

    public function createJobs(array $job)
    {
        $create_event = $this->addEvent($job);
        return true;
    }

    public function editJobs($jobs_id, $job)
    {
        return $this->jobs->updateJobs($jobs_id,$job);
    }

    public function deleteEvent(array $job_id)
    {
        // TODO: Implement deleteEvent() method.
    }

    private function addEvent($event)
    {
        return $this->jobs->createJobs($event);
        $isRecurring = false;
        if (isset($event[$this->mappings['rrule']])) {
            if ($event[$this->mappings['rrule']] !== '') {
                $isRecurring = true;
            }
        }
        if ($isRecurring) {
            if(!isset($event[$this->mappings['r_end_date']] )  ){
                $event[$this->mappings['r_end_date']] = null;
            }
            // If this is a recurring event, first calculate the duration between
            // the start and end datetimes so that each recurring instance can
            // be properly calculated.
            $event['duration'] = $this->calculateDuration(
                $event[$this->mappings['start_date']], $event[$this->mappings['end_date']]);


            // Now that duration is set, we have to update the event end date to
            // match the recurrence pattern end date (or max date if the recurring
            // pattern does not end) so that the stored record will be returned for
            // any query within the range of recurrence.
//            $r_end_date =
//            dd('here');
            $r_end_date = Carbon::parse( $this->calculateEndDate($event))->format('Y-m-d H:i:s');
//dd($r_end_date);
            $event[$this->mappings['r_end_date']] = $r_end_date ;
        }

        $event = $this->cleanEvent($event);
        return $this->jobs->createJobs($event);
    }

    /**
     * Helper method that updates the UNTIL portion of a recurring event's RRULE
     * such that the passed end date becomes the new UNTIL value. It handles updating
     * an existing UNTIL value or adding it if needed so that there is only one
     * unqiue UNTIL value when this method returns.
     */
    private function endDateRecurringSeries($event, $endDate)
    {
        $event[$this->mappings['r_end_date']] = $endDate->format('Y-m-d H:i:s');
        $parts = explode(';', $event[$this->mappings['rrule']]);
        $newRrule = array();
//        $untilFound = false;
        foreach ($parts as $key => $part) {
//            if (strrpos($part, 'UNTIL=') === false) {
//                array_push($newRrule, $part);
//            }

            if (strrpos($part, 'UNTIL=') !== false || strrpos($part, 'COUNT=') !== false || empty($part)) {
                unset($parts[$key]);
            }
        }
        $newRrule =  $parts = array_unique($parts);
        array_push($newRrule, 'UNTIL=' . $endDate->format($this->rrule_date_format));
//        $event[$this->mappings['r_end_date']] = Carbon::parse()
//        dd($event);
        $event[$this->mappings['rrule']] = implode(';', $newRrule);
        return $event;
    }


    /**
     * Returns the duration of the event in minutes
     */
    private function calculateDuration($startDate, $endDate)
    {
        $start = Carbon::parse($startDate);
        $end = Carbon::parse($endDate);
        return  $minutes = $start->diffInMinutes($end,false);

    }

    /**
     * Remove any extra attributes that are not mapped to db columns for persistence
     * otherwise MySQL will throw an error
     */
    private function cleanEvent($event)
    {
        unset($event[$this->mappings['orig_event_id']]);
        unset($event[$this->mappings['recur_edit_mode']]);
        unset($event[$this->mappings['recur_instance_start']]);
        unset($event[$this->mappings['recur_series_start']]);
        return $event;
    }

    /**
     * If the event is recurring, this function calculates the best
     * possible end date to use for the series. It will attempt to calculate
     * an end date from the RRULE if possible, and will fall back to the PHP
     * max date otherwise. The generateInstances function will still limit
     * the results regardless. For non-recurring events, it simply returns
     * the existing end date value as-is.
     */
    private function calculateEndDate($event)
    {
        $end = isset($event[$this->mappings['r_end_date']])?$event[$this->mappings['r_end_date']]:'';
        $rrule = $event[$this->mappings['rrule']];
        $isRecurring = isset($rrule) && $rrule !== '';
        if ($isRecurring) {
            $max_date = new \DateTime('9999-12-31');
            $recurrence = new BaseEvent();
//            dd($rrule);
            $recurrence->rrule($rrule);
            if (isset($recurrence->end_date) && $recurrence->end_date < $max_date) {
                // The RRULE includes an explicit end date, so use that
                $end = $recurrence->end_date->format($this->date_format) . 'Z';
            } else if (isset($recurrence->count) && $recurrence->count > 0) {
                // The RRULE has a limit, so calculate the end date based on the instance count
                $count = 0;
                $rdates = $recurrence->recur($event[$this->mappings['start_date']]);


                while ($rdate = $rdates->next()) {
                    $newEnd = $rdate;
                    if (++$count > $recurrence->count) {
                        break;
                    }
                }
                // The 'minutes' portion should match Extensible.calendar.data.EventModel.resolution:
                $newEnd->modify('+' . $event[$this->mappings['duration']] . ' minutes');
                $end = $newEnd->format($this->date_format) . 'Z';

            } else {
//                dd('here');
                // The RRULE does not specify an end date or count, so default to max date
                $end = date($this->date_format, mktime(0, 0, 0, 12, 31, 9999)) . 'Z';
            }
        }
        return $end;
    }


}
