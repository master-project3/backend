<?php


namespace App\JobsClass;


interface JobsInterface
{
    /**
     * Fetch specific event details along with its reminders and recurring patterns if available
     * @param $jobs_id
     * @return mixed
     */
    public function getSpecificJobs($jobs_id);

    /**
     * fetch specific user events based on the start and end date
     * @param $client_id
     * @param null $start_date
     * @param null $end_date
     * @return mixed
     */
    public function getSpecificClientJobs($client_id,$start_date ,$end_date);

    /**
     * create an event along with  recurring patterns if passed
     * @param array $job
     * @return mixed
     */
    public function createJobs(array $job);

    /**
     * Modify an event along with reminders and recurring patterns if passed
     * @param $jobs_id
     * @param array $job
     * @return mixed
     */
    public function editJobs($jobs_id,  $job);

    /**
     * Delete an events and all of its reminders and patterns
     * @param $job_id
     * @return mixed
     */
    public function deleteEvent(array $job_id);
}
