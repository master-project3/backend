<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Clients extends Model
{
    use HasFactory;

    protected $table = 'clients';
    protected $fillable = ['name','email','abn','address','contact_no','url','status'];
    protected $appends = array('outstanding_payment');

    public function getOutstandingPaymentAttribute(){
        $client_id = $this->attributes['id'];
        $amount = Invoice::where('client_id',$client_id)->where('status',"pending")->sum('amount');

        return '$'.$amount;
    }

    public function jobs(){
        return $this->hasMany(Jobs::class);
    }


}
