<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeeTimesheet extends Model
{
    use HasFactory;
    protected $table = 'employee_timesheets';
    protected $fillable = [
        "employee_id",
        "job_id",
        "start_date_time",
        "end_date_time",
        "is_special_shift",
        "special_rate",
        "is_processed"
    ];
    public function job(){
        return $this->belongsTo(Jobs::class);
    }
    public function employee(){
        return $this->belongsTo(Employees::class);
    }

    public function setSpecialRateAttribute($value){
        if(!isset($value) || is_null($value) || $value == ''){
            $this->attributes['special_rate'] = 0;
        }
        else{
            $this->attributes['location'] = $value;
        }
    }


}
