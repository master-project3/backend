<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employees extends Model
{
    use HasFactory;
    protected $table = 'employees';
    protected $appends = ["category_name"];
    protected $hidden = ['pivot'];
    protected $fillable = ['name','email','category_id','skill','description','address','base_rate','frequency','photo','license_number','status','bsb_number','account_number','phone_number'];
    public function availabilities(){
        return $this->hasMany(Employees_Avaibility::class);
    }

    public function job()
    {
        return $this->belongsToMany(Jobs::class,'employee_jobs','employee_id','job_id');
    }
    public function category(){

    }

    public function getPhotoAttribute($value){

        return $value !=""? url($value):'';
    }
    public function getCategoryNameAttribute(){
        $category = Category::find($this->attributes['category_id']);
        return $category->name;
    }

}
