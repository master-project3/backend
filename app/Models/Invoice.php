<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    use HasFactory;
    protected $table = 'invoices';
    public $timestamps = true;
    protected $fillable = [
        'client_id','job_id','status','description','amount','period_for_start_date','period_for_end_date'
    ];

    public function client()
    {
        return $this->belongsTo(Clients::class);
    }
    public function job()
    {
        return $this->belongsTo(Jobs::class);
    }
}
