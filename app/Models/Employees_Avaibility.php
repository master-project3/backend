<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employees_Avaibility extends Model
{
    use HasFactory;
    protected $table = 'employees_avaibilities';
    protected $fillable = [
        "monday",
        "sunday",
        "tuesday",
        "wednesday",
        "thursday",
        "friday",
        "saturday",
        "status",
        "availability_for_start_date",
        "availability_for_end_date",
        "employee_id"
    ];

    public function setEmployeeHourAttribute($value){
        $this->attributes['employee_hour'] = serialize($value);
        return true;
    }

    public function getEmployeeHourAttribute($value){
       return unserialize($value);
    }

    public function employee()
    {
        return $this->belongsTo(Employees::class);
    }

}
