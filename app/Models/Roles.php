<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    use HasFactory;
    protected $table = 'roles';
    protected $hidden = ['pivot'];

    protected $fillable = [
        'name',
        'display_name',
        'description'
    ];


    public function roles(){
        return $this->belongsToMany(User::class);
    }

}
