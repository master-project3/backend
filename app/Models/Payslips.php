<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payslips extends Model
{
    use HasFactory;
    protected $table = 'payslips';
    protected $hidden = ['pivot'];
    protected $fillable = [
        'job_id' ,
        'employee_id' ,
        'status',
        'description' ,
        'amount',
        'period_for_start_date',
        'period_for_end_date'
    ];

    public function employee()
    {
        return $this->belongsTo(Employees::class,);
    }


}
