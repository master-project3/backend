<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Recurr\Rule;
use Recurr\Transformer\TextTransformer;

class Jobs extends Model
{
    protected $table = 'jobs';
    protected $appends = array('formatted_rrule',"category_name");
    protected $attributes = [
        'rrule' => '',
    ];
    protected $hidden = ['pivot'];

    protected $fillable = [
        'title',
        'description','start_date','end_date',
        'duration','rrule','is_full_day_event',
        'is_recurring','created_by',
        'location',
        'category_id',
        'note','status','recurring_end_date',
        'client_id','no_of_employee',
        'paid_frequency','base_rate','is_accepted'
    ];

    public function client()
    {
        return $this->belongsTo(Clients::class);
    }

    public function employee()
    {
        return $this->belongsToMany(Employees::class,'employee_jobs','job_id','employee_id');
    }

    public function setRruleAttribute($value){

        if(empty($value) || is_null($value)  || $value == ''){
            $this->attributes['rrule'] = '';
        }
        else{
            $this->attributes['rrule'] = $value;
        }

    }

    public function setCreatedByAttribute($value){

        if(empty($value) || is_null($value)  || $value == ''){
            $this->attributes['created_by'] = '';
        }
        else{
            $this->attributes['created_by'] = $value;
        }

    }
    public function setNoteAttribute($value){
        if(empty($value) || !isset($value) || $value == ''){
            $this->attributes['note'] = '';
        }
        else{
            $this->attributes['note'] = $value;
        }

    }

    public function setDescriptionAttribute($value){
        if(empty($value) || !isset($value) || $value == ''){
            $this->attributes['description'] = '';
        }
        $this->attributes['description'] = $value;


    }

    public function setLocationAttribute($value){
        if(!isset($value) || is_null($value) || $value == ''){
            $this->attributes['location'] = '';
        }
        else{
            $this->attributes['location'] = $value;
        }

    }

    public function getFormattedRruleAttribute(){
        if(empty($this->attributes['rrule']) || is_null($this->attributes['rrule'])) return [];
        $total_rrule = explode('RRULE:',$this->attributes['rrule']);
        $count = count($total_rrule);
        if($count == 2){
            $rrule = $total_rrule[1];
        }
        if($count == 1){
            $rrule = $total_rrule[0];
        }
        if($count > 2){
            return false;
        }
        $rule = new Rule($rrule, null,null,null);
        $textTransformer = new TextTransformer();
        $data['human_readable'] = $textTransformer->transform($rule);
        $data['recurring_type'] = strtolower($rule->getFreqAsText());
        if($data['recurring_type'] == 'yearly'){
            $data['recurring_by_month'] = implode(',',$rule->getByMonth());
            if(!is_null($rule->getByMonthDay())){
                $data['recurring_by_month_day'] = implode(',',$rule->getByMonthDay());
            }
            if(!is_null($rule->getByDay())){
                $data['recurring_by_day'] = $this->getDaysInNumeric($rule->getByDay());
            }
            if(!is_null($rule->getUntil())){
                $data['recurring_by_until'] = $rule->getUntil();
            }
            if(!is_null($rule->getCount())){
                $data['recurring_count'] = $rule->getCount();
            }
            if(!is_null($rule->getInterval())){
                $data['recurring_interval'] = $rule->getInterval();
            }
        }
        elseif ($data['recurring_type'] == 'monthly'){
            if(!is_null($rule->getUntil())){
                $data['recurring_by_until'] = $rule->getUntil();
            }

            if(!is_null($rule->getByMonthDay())){
                $data['recurring_by_month_day'] = implode(',',$rule->getByMonthDay());
            }

            if(!is_null($rule->getCount())){
                $data['recurring_count'] = $rule->getCount();
            }
            if(!is_null($rule->getInterval())){
                $data['recurring_interval'] = $rule->getInterval();
            }
            if(!is_null($rule->getByDay())){
                $data['recurring_by_day'] = $this->getDaysInNumeric($rule->getByDay());
            }
            if(!is_null($rule->getBySetPosition())){
                $data['recurring_by_set_pos'] = $rule->getBySetPosition();
            }
        }
        elseif ( $data['recurring_type'] == 'weekly'){
            $data['recurring_by_day'] = $this->getDaysInNumeric($rule->getByDay());
            if(!is_null($rule->getInterval())){
                $data['recurring_interval'] = $rule->getInterval();
            }
            if(!is_null($rule->getCount())){
                $data['recurring_count'] = $rule->getCount();
            }
            if(!is_null($rule->getUntil())){
                $data['recurring_by_until'] = $rule->getUntil();
            }
        }
        else{
            if(!is_null($rule->getInterval())){
                $data['recurring_interval'] = $rule->getInterval();
            }
            if(!is_null($rule->getCount())){
                $data['recurring_count'] = $rule->getCount();
            }
            if(!is_null($rule->getUntil())){
                $data['recurring_by_until'] = $rule->getUntil();
            }
        }
        return $data;
    }

    //convert week days into numeric like 0 for SU,1 for MO
    private function getDaysInNumeric(array $week_days){
        $days = [
            'SU',"MO","TU","WE","TH","FR","SA"
        ];
        $new_numeric_day = [];
        foreach ($week_days as $day){
            $new_numeric_day[] = array_search($day,$days);
        }
        return $new_numeric_day;
    }
    

    public function getCategoryNameAttribute(){
        $category = Category::find($this->attributes['category_id']);
        return $category->name;
    }

}
