<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
Route::get('transaction/history',[\App\Http\Controllers\DashboardController::class,"getTransactionHistory"]);
Route::post('login',[\App\Http\Controllers\AuthController::class,'login'])->name('login');
Route::get('pay-now',[\App\Http\Controllers\PayrollController::class,'payNow']);
Route::get('reports',[\App\Http\Controllers\DashboardController::class,'getReport']);
Route::get('invoices',[\App\Http\Controllers\InvoiceGenerationCrontroller::class,'listAllInvoice']);
Route::get('payslips/histories',[\App\Http\Controllers\PayrollController::class,'listAllPayroll']);
Route::get('generate/invoice',[\App\Http\Controllers\InvoiceGenerationCrontroller::class,'generateInvoice']);
Route::get('categories',[\App\Http\Controllers\EmployeeController::class,'getCategoryList']);
Route::post('categories',[\App\Http\Controllers\CategoryController::class,'createCategory']);
Route::put('categories/{id}',[\App\Http\Controllers\CategoryController::class,'updateCategory'])->where([
    'id' => '[0-9]+'
]);
Route::delete('categories/{id}',[\App\Http\Controllers\CategoryController::class,'deleteCategory'])->where([
    'id' => '[0-9]+'
]);
Route::post('reset/password',[\App\Http\Controllers\UserController::class,'resetPassword']);

Route::group(['middleware' => 'auth'], function() {

    Route::post('pay/employee',[\App\Http\Controllers\PayrollController::class,'payToEmployee'])->middleware('verifyRole:admin|payroll-officer');

    /**
     *
     */
    Route::get('payrolls',[\App\Http\Controllers\PayrollController::class,'listPayroll'])->middleware('verifyRole:admin|payroll-officer');


    /**
     *  Logged in user details
     */
    Route::get('users',[\App\Http\Controllers\UserController::class,'getLoggedInUserDetails'])->where([
        'id' => '[0-9]+'
    ])->middleware('verifyRole:admin|service-supervisor|payroll-officer|staff-manager');



    Route::put('users/{id}',[\App\Http\Controllers\UserController::class,'updateLoggedInuserProfile'])->where([
        'id' => '[0-9]+'
    ])->middleware('verifyRole:admin|service-supervisor|payroll-officer|staff-manager');;
    /**
     * Admin update profile
     */
    Route::get('admin/users',[\App\Http\Controllers\UserController::class,'listAllUser'])->middleware('verifyRole:admin');;
    Route::post('admin/users',[\App\Http\Controllers\UserController::class,'createUser'])->middleware('verifyRole:admin');
    Route::put('admin/users/{id}',[\App\Http\Controllers\UserController::class,'updateUserDetail'])->where([
        'id' => '[0-9]+'
    ])->middleware('verifyRole:admin');
    Route::delete('admin/users/{id}',[\App\Http\Controllers\UserController::class,'deleteUser'])->where([
        'id' => '[0-9]+'
    ])->middleware('verifyRole:admin');
    Route::get('admin/users/{id}',[\App\Http\Controllers\UserController::class,'getSpecificUserInfo'])->where([
        'id' => '[0-9]+'
    ])->middleware('verifyRole:admin');

    Route::get('admin/roles',[\App\Http\Controllers\RoleController::class,'listAllRoles'])->middleware('verifyRole:admin');;
    Route::post('admin/roles',[\App\Http\Controllers\RoleController::class,'storeRole'])->middleware('verifyRole:admin');
    Route::put('admin/roles/{id}',[\App\Http\Controllers\RoleController::class,'updateRole'])->where([
        'id' => '[0-9]+'
    ])->middleware('verifyRole:admin');


    /**
     * Client Crud starts here
     */
    Route::post('clients', [\App\Http\Controllers\ClientsController::class, 'createClient'])->middleware('verifyRole:service-supervisor|admin');

    Route::put('clients/{id}', [\App\Http\Controllers\ClientsController::class, 'updateClient'])->where([
        'id' => '[0-9]+'
    ])->middleware('verifyRole:service-supervisor|admin');



    Route::get('mail/clients/{id}', [\App\Http\Controllers\ClientsController::class, 'sendEmail'])->where([
        'id' => '[0-9]+'
    ])->middleware('verifyRole:service-supervisor|admin');

    Route::get('clients/{id}', [\App\Http\Controllers\ClientsController::class, 'getClient'])->where([
        'id' => '[0-9]+'
    ])->middleware('verifyRole:service-supervisor|admin');

    Route::get('clients', [\App\Http\Controllers\ClientsController::class, 'listAllClients'])->where([
        'id' => '[0-9]+'
    ])->middleware('verifyRole:service-supervisor|admin');
    Route::delete('clients/{id}', [\App\Http\Controllers\ClientsController::class, 'deleteCLient'])->middleware('verifyRole:service-supervisor|admin');

    /**
     * Clients crud ends here
     */

    /**
     * Jobs CRUD starts here
     */
    Route::post('jobs', [\App\Http\Controllers\JobsController::class, 'createJobs'])->middleware('verifyRole:service-supervisor|admin');

    Route::put('jobs/{id}', [\App\Http\Controllers\JobsController::class, 'updateJobs'])->where([
        'id' => '[0-9]+'
    ])->middleware('verifyRole:service-supervisor|admin');


    Route::get('jobs/{id}', [\App\Http\Controllers\JobsController::class, 'getSpecificJob'])->where([
        'id' => '[0-9]+'
    ])->middleware('verifyRole:service-supervisor|admin');

    Route::get('jobs', [\App\Http\Controllers\JobsController::class, 'listAllJobs'])->middleware('verifyRole:service-supervisor|admin');
    /**
     * Job crud ends here
     */


    /**
     * Employee CRUD starts here
     */
    Route::post('employees', [\App\Http\Controllers\EmployeeController::class, 'createEmployee'])->middleware('verifyRole:staff-manager|admin');

    Route::put('employees/{id}', [\App\Http\Controllers\EmployeeController::class, 'updateEmployee'])->where([
        'id' => '[0-9]+'
    ])->middleware('verifyRole:staff-manager|admin');


    Route::get('employees/{id}', [\App\Http\Controllers\EmployeeController::class, 'getEmployeeDetails'])->where([
        'id' => '[0-9]+'
    ])->middleware('verifyRole:staff-manager|admin');

    Route::get('employees', [\App\Http\Controllers\EmployeeController::class, 'listAllEmployee'])->middleware('verifyRole:staff-manager|admin');
    /**
     * Create employee avaibility
     */
    Route::post('employees/avaibility', [\App\Http\Controllers\EmployeeAvailabilityController::class, 'createAvaibility'])->middleware('verifyRole:staff-manager|admin');

    /**
     * update employee avaibility
     */
    Route::put('employees/avaibility/{id}', [\App\Http\Controllers\EmployeeAvailabilityController::class, 'updateAvaibility'])->where([
        'id' => '[0-9]+'
    ])->middleware('verifyRole:staff-manager|admin');


    /**
     * list all employee avaibility
     */
    Route::get('employees/avaibility', [\App\Http\Controllers\EmployeeAvailabilityController::class, 'listAllEmployeeAvaibility'])->middleware('verifyRole:staff-manager|admin');

    /**
     * Employee Timesheets
     */

    Route::post('employees/timesheets', [\App\Http\Controllers\EmployeeTimesheetController::class, 'createTimesheet'])->middleware('verifyRole:staff-manager|admin');

    Route::put('employees/timesheets/{id}', [\App\Http\Controllers\EmployeeTimesheetController::class, 'updateTimesheet'])->where([
        'id' => '[0-9]+'
    ])->middleware('verifyRole:staff-manager|admin');


    Route::get('employees/timesheets/{id}', [\App\Http\Controllers\EmployeeTimesheetController::class, 'getSpecificDateTimesheet'])->where([
        'id' => '[0-9]+'
    ])->middleware('verifyRole:staff-manager|admin');

    Route::get('employees/timesheets', [\App\Http\Controllers\EmployeeTimesheetController::class, 'getSpecialEmployeeTimesheet'])->middleware('verifyRole:staff-manager|admin');

/**
 * Assigning/Unassigning/fetching job assigned to employee
 */

Route::get('assign/employees',[
        \App\Http\Controllers\JobAssignmentController::class,
        'getAssignedJob'
    ])->middleware('verifyRole:staff-manager|admin');

Route::post('assign/employees',[
    \App\Http\Controllers\JobAssignmentController::class,
    'assignJobs'
])->middleware('verifyRole:staff-manager|admin');

    Route::put('unassign/employees',[
        \App\Http\Controllers\JobAssignmentController::class,
        'removeJobs'
    ])->middleware('verifyRole:staff-manager|admin');
});
